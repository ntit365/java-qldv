/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Common.DBConnection;
import Model.NganhHoc;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author truon
 */
public class NganhHocDAO implements DAOImplement<NganhHoc>{
    private Statement statement = null;
    private ResultSet resultSet = null;
    private PreparedStatement preparedStatement = null;
    List<NganhHoc> listNganhHoc = null;

    @Override
    public List<NganhHoc> getAll() {
        listNganhHoc = new LinkedList<>();
        try {
            statement = DBConnection.createConnection().createStatement();
            resultSet = statement.executeQuery("SELECT * FROM nganhhoc");
            while (resultSet.next()) {
                NganhHoc nganhHoc = new NganhHoc();
                nganhHoc.setId(resultSet.getInt("id"));
                nganhHoc.setMaNganhHoc(resultSet.getString("ma_nganh_hoc"));
                nganhHoc.setTenNganhHoc(resultSet.getString("ten_nganh_hoc"));
                nganhHoc.setIdKhoa(resultSet.getInt("id_khoa"));
                nganhHoc.setTenKhoa(getKhoaName(resultSet.getInt("id_khoa")));
                listNganhHoc.add(nganhHoc);
            }
            resultSet.close();
            statement.close();
            DBConnection.closeConnection();
        } catch (SQLException ex) {
        }
        return listNganhHoc;
    }

    @Override
    public Optional<NganhHoc> get(int id) {
        if (listNganhHoc == null) listNganhHoc = this.getAll();
        return listNganhHoc.stream().filter(s -> s.getId() == id).findFirst();
    }

    @Override
    public int save(NganhHoc nganhHoc) {
        try {
            if (listNganhHoc == null) listNganhHoc = this.getAll();
            if (!listNganhHoc.stream().anyMatch((s) -> (s.getMaNganhHoc()== null ? nganhHoc.getMaNganhHoc() == null : s.getMaNganhHoc().equals(nganhHoc.getMaNganhHoc())))) {
                preparedStatement = DBConnection.createConnection().prepareStatement("INSERT INTO nganhhoc (id, id_khoa, ma_nganh_hoc, ten_nganh_hoc) VALUES  (NULL,?,?,?)");
                preparedStatement.setInt(1, nganhHoc.getIdKhoa());
                preparedStatement.setString(2, nganhHoc.getMaNganhHoc());
                preparedStatement.setString(3, nganhHoc.getTenNganhHoc());
                int result = preparedStatement.executeUpdate();
                DBConnection.closeConnection();
                if (result < 0) {
                    return ERROR;
                } else {
                    return SUCCESS;
                }
            } else {
                return DUPLICATE_RESULT;
            }
        } catch (SQLException ex) {
            return ERROR;
        }
    }

    @Override
    public int update(NganhHoc nganhHoc) {
        try {
            if (listNganhHoc == null) listNganhHoc = this.getAll();
            if (!listNganhHoc.stream().anyMatch((s) -> s.getId() != nganhHoc.getId() && (s.getMaNganhHoc()== null ? nganhHoc.getMaNganhHoc()== null : s.getMaNganhHoc().equals(nganhHoc.getMaNganhHoc())))) {
                preparedStatement = DBConnection.createConnection().prepareStatement("UPDATE nganhhoc SET id_khoa = ?, ten_nganh_hoc = ? WHERE id = ? AND ma_nganh_hoc = ?");
                preparedStatement.setInt(1, nganhHoc.getIdKhoa());
                preparedStatement.setString(2, nganhHoc.getTenNganhHoc());
                preparedStatement.setInt(3, nganhHoc.getId());
                preparedStatement.setString(4, nganhHoc.getMaNganhHoc());
                int result = preparedStatement.executeUpdate();
                DBConnection.closeConnection();
                if (result < 0) {
                    return ERROR;
                } else {
                    return SUCCESS;
                }
            } else {
                return DUPLICATE_RESULT;
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
            return ERROR;
        }
    }

    @Override
    public int delete(NganhHoc nganhHoc) {
        try {
            preparedStatement = DBConnection.createConnection().prepareStatement("DELETE FROM nganhhoc WHERE id = ?");
            preparedStatement.setInt(1, nganhHoc.getId());
            int result = preparedStatement.executeUpdate();
            if (result < 0) {
                return ERROR;
            } else {
                return SUCCESS;
            }
        } catch (SQLException e) {
            return ERROR;
        }
    }
    
    public List<NganhHoc> getNganhHocWithIdKhoa(int idKhoa){
        if(listNganhHoc == null) listNganhHoc=this.getAll();
        return listNganhHoc.stream().filter(u -> u.getIdKhoa()==idKhoa).collect(Collectors.toList());
    }
    
    public String getKhoaName(int idKhoa) {
        KhoaDAO khoaDAO = new KhoaDAO();
        return khoaDAO.get(idKhoa).get().getTenKhoa();
    }
}
