/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Common.DBConnection;
import Model.Khoa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author truon
 */
public class KhoaDAO implements DAOImplement<Khoa>{
    
    private Connection cnn = null;
    private Statement statement = null;
    private ResultSet resultSet = null;
    private PreparedStatement preparedStatement = null;
    
    private List<Khoa> listKhoas = null;

    @Override
    public List<Khoa> getAll() {
        listKhoas = new LinkedList<>();
        try {
            statement = DBConnection.createConnection().createStatement();
            resultSet = statement.executeQuery("SELECT * FROM khoa");
            while (resultSet.next()) {
                Khoa khoa = new Khoa();
                khoa.setId(resultSet.getInt("id"));
                khoa.setMaKhoa(resultSet.getString("ma_khoa"));
                khoa.setTenKhoa(resultSet.getString("ten_khoa"));
                listKhoas.add(khoa);
            }
            resultSet.close();
            statement.close();
            DBConnection.closeConnection();
        } catch (SQLException ex) {
        }
        return listKhoas;
    }

    @Override
    public Optional<Khoa> get(int id) {
        if (listKhoas == null) {
            listKhoas = this.getAll();
        }
        return listKhoas.stream().filter( s -> s.getId() == id).findFirst();
    }

    @Override
    public int save(Khoa khoa) {
        if (listKhoas == null) {
            listKhoas = this.getAll();
        }
        try {
            if (!listKhoas.stream().anyMatch((s) -> (s.getMaKhoa()== null ? khoa.getMaKhoa() == null : s.getMaKhoa().equals(khoa.getMaKhoa())))) {
                preparedStatement = DBConnection.createConnection().prepareStatement("INSERT INTO khoa (id, ma_khoa, ten_khoa) VALUES  (NULL,?,?)");
                preparedStatement.setString(1, khoa.getMaKhoa());
                preparedStatement.setString(2, khoa.getTenKhoa());
                int result = preparedStatement.executeUpdate();
                DBConnection.closeConnection();
                if ( result < 0) {
                    return ERROR;
                } else {
                    return SUCCESS;
                }

            } else {
                return DUPLICATE_RESULT;
            }
        } catch (SQLException ex) {
            return ERROR;
        }
    }

    @Override
    public int update(Khoa khoa) {
        if (listKhoas == null) {
            listKhoas = this.getAll();
        }
        try {
            if (!listKhoas.stream().anyMatch((s) -> s.getId() != khoa.getId() && (s.getMaKhoa()== null ? khoa.getMaKhoa()== null : s.getMaKhoa().equals(khoa.getMaKhoa())))) {
                preparedStatement = DBConnection.createConnection().prepareStatement("UPDATE khoa SET ten_khoa = ? WHERE id = ? AND ma_khoa = ?");
                preparedStatement.setString(1, khoa.getTenKhoa());
                preparedStatement.setInt(2, khoa.getId());
                preparedStatement.setString(3, khoa.getMaKhoa());
                int result = preparedStatement.executeUpdate();
                DBConnection.closeConnection();
                if ( result < 0) {
                    return ERROR;
                } else {
                    return SUCCESS;
                }
            } else {
                return DUPLICATE_RESULT;
            }
        } catch (SQLException e) {
            return ERROR;
        }
    }

    @Override
    public int delete(Khoa khoa) {
        try {
            preparedStatement = DBConnection.createConnection().prepareStatement("DELETE FROM khoa WHERE id = ?");
            preparedStatement.setInt(1, khoa.getId());
            int result = preparedStatement.executeUpdate();
            DBConnection.closeConnection();
            if (result < 0) {
                return ERROR;
            } else {
                return SUCCESS;
            }
        } catch (SQLException e) {
            return ERROR;
        }
    }
    
    public List<Khoa> getKhoaWithMaKhoa(String maKhoa)
    {
        if (listKhoas==null) listKhoas = this.getAll();
        return listKhoas.stream().filter(u -> u.getMaKhoa().equals(maKhoa)).collect(Collectors.toList());
    }
    
}
