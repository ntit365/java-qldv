/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Common.DBConnection;
import Model.Province;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author truon
 */
public class ProvinceDAO{

    private final Connection cnn = DBConnection.createConnection();
    private Statement statement = null;
    private ResultSet resultSet = null;
    List <Province> listProvinces = null;
    public List<Province> getAll() {
        listProvinces = new LinkedList<>();
        try {
            statement = cnn.createStatement();
            resultSet = statement.executeQuery("select * from province");
            while (resultSet.next()) {
                Province p = new Province();
                p.setId(resultSet.getString("matp"));
                p.setName(resultSet.getString("name"));
                p.setType(resultSet.getString("type"));
                listProvinces.add(p);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            
        }
        return listProvinces;
    }
    
    public Province getProvinceWithId(String id) {
       Province province = null;
        try {
            province = new Province();
            statement = cnn.createStatement();
            resultSet = statement.executeQuery("select * from province where matp = " + id);
            while (resultSet.next()) {
                province.setId(resultSet.getString("matp"));
                province.setName(resultSet.getString("name"));
                province.setType(resultSet.getString("type"));
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            
        }
        return province; 
    }
    
}
