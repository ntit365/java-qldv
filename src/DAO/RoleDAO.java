/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Common.DBConnection;
import Model.Role;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author truon
 */
public class RoleDAO implements DAOImplement<Role>{
    
    private Statement statement = null;
    private ResultSet resultSet = null;
    private PreparedStatement preparedStatement = null;
    
    private List<Role> listRoles = null;
    
    @Override
    public List<Role> getAll() {
        listRoles = new LinkedList<>();
        try {
            statement = DBConnection.createConnection().createStatement();
            resultSet = statement.executeQuery("select * from role");
            while (resultSet.next()) {
                Role role = new Role();
                role.setId(resultSet.getInt("id"));
                role.setLabel(resultSet.getString("label"));
                role.setName(resultSet.getString("name"));
                role.setPermissons(resultSet.getString("permissions"));
                listRoles.add(role);
            }
            resultSet.close();
            statement.close();
            DBConnection.closeConnection();
        } catch (SQLException e) {
            
        }
        return listRoles;
    }

    @Override
    public Optional<Role> get(int id) {
        if (listRoles == null) {
            listRoles = this.getAll();
        } 
        return listRoles.stream().filter(u -> u.getId() == id).findFirst();
    }

    @Override
    public int save(Role role) {
        if (listRoles == null) {
            listRoles = this.getAll();
        }
        try {
            if (!listRoles.stream().anyMatch((s) -> (s.getName()== null ? role.getName()== null : s.getName().equals(role.getName())))) {
                preparedStatement = DBConnection.createConnection().prepareStatement("INSERT INTO role (id, name, label, permissions) VALUES  (NULL,?,?,?)");
                preparedStatement.setString(1, role.getName());
                preparedStatement.setString(2, role.getLabel());
                preparedStatement.setString(3, role.getPermissons());
                int result = preparedStatement.executeUpdate();
                DBConnection.closeConnection();
                if (result < 0) {
                    return ERROR;
                } else {
                    return SUCCESS;
                }
            } else {
                return DUPLICATE_RESULT;
            }
        } catch (SQLException ex) {
            return ERROR;
        }
    }

    @Override
    public int update(Role role) {
        if (listRoles == null) {
            listRoles = this.getAll();
        }
        try {
            if (!listRoles.stream().anyMatch((s) -> s.getId() != role.getId() && (s.getName()== null ? role.getName()== null : !role.getName().equals(role.getName())))) {
                preparedStatement = DBConnection.createConnection().prepareStatement("UPDATE role SET name = ?, label = ?, permissions = ? WHERE id = ?");
                preparedStatement.setString(1, role.getName());
                preparedStatement.setString(2, role.getLabel());
                preparedStatement.setString(3, role.getPermissons());
                preparedStatement.setInt(4, role.getId());
                int result = preparedStatement.executeUpdate();
                DBConnection.closeConnection();
                if (result < 0) {
                    return ERROR;
                } else {
                    return SUCCESS;
                }
            } else {
                return DUPLICATE_RESULT;
            }
        } catch (SQLException ex) {
            return ERROR;
        }
    }

    @Override
    public int delete(Role role) {
        try {
            preparedStatement = DBConnection.createConnection().prepareStatement("DELETE FROM role WHERE id = ?");
            preparedStatement.setInt(1, role.getId());
            int result = preparedStatement.executeUpdate();
            if (result < 0) {
                return ERROR;
            } else {
                return SUCCESS;
            }
        } catch (SQLException e) {
            return ERROR;
        }
    }
}
