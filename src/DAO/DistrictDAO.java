/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Common.DBConnection;
import Model.District;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author truon
 */
public class DistrictDAO {
    
    private Connection cnn = null;
    private Statement statement = null;
    private ResultSet resultSet = null;
    private List<District> listDistricts = null;
    public List<District> getAll() {
        cnn = DBConnection.createConnection();
        listDistricts = new LinkedList<>();
        try {
            statement = cnn.createStatement();
            resultSet = statement.executeQuery("select * from district");
            while (resultSet.next()) {
                District d = new District();
                d.setId(resultSet.getString("maqh"));
                d.setName(resultSet.getString("name"));
                d.setType(resultSet.getString("type"));
                d.setIdProvince(resultSet.getString("matp"));
                listDistricts.add(d);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
        }
        DBConnection.closeConnection();
        return listDistricts;
    }
    
    public District getDistrictWithId(String id) {
        District district = new District();
        cnn = DBConnection.createConnection();
        try {
            statement = cnn.createStatement();
            resultSet = statement.executeQuery("select * from district where maqh = " + id);
            while (resultSet.next()) {
                district.setId(resultSet.getString("maqh"));
                district.setName(resultSet.getString("name"));
                district.setType(resultSet.getString("type"));
                district.setIdProvince(resultSet.getString("matp"));
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
        }
        DBConnection.closeConnection();
        return district; 
    }
    
    public List<District> getDistrictWithIdProvince(String idProvince)
    {
        if (listDistricts == null ) listDistricts=this.getAll();
        return listDistricts.stream().filter(u -> u.getIdProvince().equals(idProvince)).collect(Collectors.toList());
    }
}
