/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Common.DBConnection;
import Model.Permission;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author truon
 */
public class PermissionDAO{

    private Statement statement = null;
    private ResultSet resultSet = null;
    
    private List<Permission> listPermissions = null;
    
    public List<Permission> getAll() {
        listPermissions = new LinkedList<>();
        try {
            statement = DBConnection.createConnection().createStatement();
            resultSet = statement.executeQuery("select * from permission");
            while (resultSet.next()) {
                Permission permission = new Permission();
                permission.setId(resultSet.getInt("id"));
                permission.setLabel(resultSet.getString("label"));
                permission.setName(resultSet.getString("name"));
                listPermissions.add(permission);
            }
            resultSet.close();
            statement.close();
            DBConnection.closeConnection();
        } catch (SQLException e) {
            
        }
        return listPermissions;
    }

    public Optional<Permission> get(int id) {
        if (listPermissions == null) {
            listPermissions = this.getAll();
        }
        return listPermissions.stream().filter(u -> u.getId() == id).findFirst();
    }
    
    public Optional<Permission> getPermissionWithLabel(String label) {
        if (listPermissions == null) {
            listPermissions = this.getAll();
        }
        return listPermissions.stream().filter(u -> u.getLabel().equals(label)).findFirst();
    }
}
