/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Common.DBConnection;
import Model.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author truon
 */
public class WardDAO {

    private Statement statement = null;
    private ResultSet resultSet = null;
    List<Ward> listWards = null;
    
    public List<Ward> getAll() {
        listWards = new LinkedList<>();
        try {
            statement = DBConnection.createConnection().createStatement();
            resultSet = statement.executeQuery("select * from ward");
            while (resultSet.next()) {
                Ward w = new Ward();
                w.setId(resultSet.getString("xaid"));
                w.setName(resultSet.getString("name"));
                w.setType(resultSet.getString("type"));
                w.setIdDistrict(resultSet.getString("maqh"));
                listWards.add(w);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            
        }
        return listWards;
    }
    
    public Ward getWardWithId(String id) {
        Ward ward = new Ward();
        try {
            statement = DBConnection.createConnection().createStatement();
            resultSet = statement.executeQuery("select * from ward where xaid = " + id);
            while (resultSet.next()) {
                ward.setId(resultSet.getString("xaid"));
                ward.setName(resultSet.getString("name"));
                ward.setType(resultSet.getString("type"));
                ward.setIdDistrict(resultSet.getString("maqh"));
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            
        }
        return ward;
    }
    
    public List<Ward> getWardWithIdDistrict(String idDistrict)
    {
        if (listWards==null) listWards=this.getAll();
        return listWards.stream().filter(u -> u.getIdDistrict().equals(idDistrict)).collect(Collectors.toList());
    }
}
