/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;
import Common.DBConnection;
import Model.Khoa;
import Model.Role;
import Model.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author truon
 */
public class UserDAO implements DAOImplement<User>{  
    private PreparedStatement preparedStatement = null;
    private Statement statement = null;
    private ResultSet resultSet = null;
    
    private List<User> listUsers = null;
    
    @Override
    public List<User> getAll() {
        this.listUsers = new LinkedList<>();
        try {
            statement = DBConnection.createConnection().createStatement();
            resultSet = statement.executeQuery("SELECT * FROM users");
            while (resultSet.next()) {
                User user = new User();
                user.setId(Integer.parseInt(resultSet.getString("id")));
                user.setIdKhoa(resultSet.getInt("id_khoa"));
                user.setRoleName(getRoleName(resultSet.getInt("id_role")));
                user.setKhoaName(getKhoaName(resultSet.getInt("id_khoa")));
                user.setIdRole(resultSet.getInt("id_role"));
                user.setName(resultSet.getString("name"));
                user.setPassword(resultSet.getString("password"));
                user.setUsername(resultSet.getString("username"));
                listUsers.add(user);
            }
            resultSet.close();
            statement.close();
            DBConnection.closeConnection();
        } catch (SQLException ex) {
        }
        return listUsers;
    }
    
    public String getKhoaName(int id)
    {
        KhoaDAO khoaDAO = new KhoaDAO();
        Optional<Khoa> khoa = khoaDAO.get(id);
        return khoa.get().getTenKhoa();
    }
    
    public String getRoleName(int id)
    {
        RoleDAO roleDAO = new RoleDAO();
        Optional<Role> role = roleDAO.get(id);
        return role.get().getLabel();
    }

    @Override
    public Optional<User> get(int id) {
        if (this.listUsers == null) {
            this.listUsers = this.getAll();
        }
        return listUsers.stream().filter(u -> u.getId() == id).findFirst();
    }
    

    @Override
    public int save(User user) {
        if (this.listUsers == null) {
            this.listUsers = this.getAll();
        }
        try {
            if (!listUsers.stream().anyMatch((s) -> (s.getUsername() == null ? user.getUsername() == null : s.getUsername().equals(user.getUsername())))) {
                preparedStatement = DBConnection.createConnection().prepareStatement("INSERT INTO users (id, id_khoa, id_role, name, username, password) VALUES (NULL, ?, ?, ?, ?, ?)");
                preparedStatement.setInt(1, user.getIdKhoa());
                preparedStatement.setInt(2, user.getIdRole());
                preparedStatement.setString(3, user.getName());
                preparedStatement.setString(4, user.getUsername());
                preparedStatement.setString(5, user.getPassword());
                int result = preparedStatement.executeUpdate();
                DBConnection.closeConnection();
                if (result < 0) {
                    return ERROR;
                } else {
                    return SUCCESS;
                }
            } else {
                return DUPLICATE_RESULT;
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            return ERROR;
        }
    }

    @Override
    public int update(User user) {
        if (this.listUsers == null) {
            this.listUsers = this.getAll();
        }
        try {
            if (!listUsers.stream().anyMatch((s) -> s.getId() != user.getId() && (s.getUsername() == null ? user.getUsername() == null : s.getUsername().equals(user.getUsername())))) {
                preparedStatement = DBConnection.createConnection().prepareStatement("UPDATE users SET id_khoa= ?, name = ?, id_role = ? WHERE id = ?");
                preparedStatement.setInt(1, user.getIdKhoa());
                preparedStatement.setString(2, user.getName());
                preparedStatement.setInt(3, user.getIdRole());
                preparedStatement.setInt(4, user.getId());
                int result = preparedStatement.executeUpdate();
                DBConnection.closeConnection();
                if (result < 0) {
                    return ERROR;
                } else {
                    return SUCCESS;
                }
            } else {
                return DUPLICATE_RESULT;
            }
        } catch (SQLException e) {
            return ERROR;
        }
    }
    
    public int updateInfo(User user) {
        if (this.listUsers == null) {
            this.listUsers = this.getAll();
        }
        try {
            if (listUsers.stream().anyMatch((s) -> s.getId() == user.getId())) {
                preparedStatement = DBConnection.createConnection().prepareStatement("UPDATE users SET password = ?, name = ? WHERE id = ?");
                preparedStatement.setString(1, user.getPassword());
                preparedStatement.setString(2, user.getName());
                preparedStatement.setInt(3, user.getId());
                int result = preparedStatement.executeUpdate();
                DBConnection.closeConnection();
                if (result < 0) {
                    return ERROR;
                } else {
                    return SUCCESS;
                }
            } else {
                return DUPLICATE_RESULT;
            }
        } catch (SQLException e) {
            return ERROR;
        }
    }

    @Override
    public int delete(User user) {
        try {
            preparedStatement = DBConnection.createConnection().prepareStatement("DELETE FROM users WHERE id = ?");
            preparedStatement.setInt(1, user.getId());
            int result = preparedStatement.executeUpdate();
            DBConnection.closeConnection();
            if (result < 0) {
                return ERROR;
            } else {
                return SUCCESS;
            }
        } catch (SQLException e) {
            return ERROR;
        }
    }
}
