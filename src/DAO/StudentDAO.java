/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Common.DBConnection;
import Model.Lop;
import Model.NganhHoc;
import Model.Student;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.json.JSONObject;

/**
 *
 * @author truon
 */
public class StudentDAO implements DAOImplement<Student>{
    
    private final Connection cnn = DBConnection.createConnection();
    private Statement statement = null;
    private ResultSet resultSet = null;
    LopDAO lopDAO = new LopDAO();
    NganhHocDAO nganhHocDAO = new NganhHocDAO();
    KhoaDAO khoaDAO = new KhoaDAO();
    private List<Student> listStudents = null;
    
    @Override
    public List<Student> getAll() {
        listStudents = new LinkedList<>();
        try {
            statement = cnn.createStatement();
            resultSet = statement.executeQuery("select * from student");
            while (resultSet.next()) {
                Student st = new Student();
                st.setId(resultSet.getInt("id"));
                st.setStudentCode(resultSet.getString("student_code"));
                st.setHoTen(resultSet.getString("ho_ten"));
                st.setGioiTinh(resultSet.getString("gioi_tinh"));
                st.setNgaySinh(resultSet.getString("ngay_sinh"));
                st.setNgayVaoDoan(resultSet.getString("ngay_vao_doan"));
                st.setDienThoai(resultSet.getString("dien_thoai"));
                st.setDiaChi(getShowDiaChi(resultSet.getString("dia_chi")));
                JSONObject object = new JSONObject(resultSet.getString("dia_chi"));
                st.setIdWard(object.getString("ward"));
                st.setIdDistrict(object.getString("district"));
                st.setIdProvince(object.getString("province"));
                st.setSoDoan(resultSet.getBoolean("so_doan"));
                st.setSinhHoat(resultSet.getBoolean("sinh_hoat"));
                st.setIdLop(resultSet.getInt("id_lop"));
                st.setIdKhoaHoc(resultSet.getInt("id_khoa_hoc"));
                st.setChucVu(resultSet.getString("chuc_vu"));
                st.setDoanPhi(resultSet.getBoolean("doan_phi"));
                listStudents.add(st);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            
        }
        return listStudents;
    }

    @Override
    public Optional<Student> get(int id) {
        if (listStudents == null) {
            listStudents = this.getAll();
        }
        return listStudents.stream().filter(u -> u.getId() == id).findFirst(); 
    }
    
    
    public List<Student> getListSinhHoat(boolean isSinhHoat){
        if (listStudents == null) {
            listStudents = this.getAll();
        }
        return listStudents.stream().filter(u -> u.isSinhHoat() == isSinhHoat).collect(Collectors.toList());
    }
    
    public List<Student> getListSoDoan(boolean isSoDoan){
        if (listStudents == null) {
            listStudents = this.getAll();
        }
        return listStudents.stream().filter(u -> u.isSoDoan() == isSoDoan).collect(Collectors.toList());
    }
    
    protected String getShowDiaChi(String diaChi) {
        JSONObject object = new JSONObject(diaChi);
        String result = null;
        String query = "SELECT ward.name as 'ward', district.name as 'district', province.name as 'province' FROM province"
                + " INNER JOIN district on province.matp = district.matp"
                + " INNER JOIN ward on district.maqh = ward.maqh"
                + " WHERE province.matp = " + object.getString("province") + " AND district.maqh = " + object.getString("district") + " AND ward.xaid = " + object.getString("ward");
        try {
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                result = rs.getString("ward") + ", " + rs.getString("district") + ", " + rs.getString("province");
            }
        } catch (SQLException e) {
            
        }
        return result;
    }
    
    /**
     * Get student with student code
     * @param studentCode
     * @return 
     */
    public Optional<Student> getStutentWithStudentCode(String studentCode) {
        if (listStudents == null) listStudents = this.getAll();
        return listStudents.stream().filter(u -> u.getStudentCode().equals(studentCode)).findFirst();
    }

    /**
     * Save student to database
     * @param student
     * @return 
     */
    @Override
    public int save(Student student) {
        try {
            if (listStudents == null) {
                listStudents = this.getAll();
            }
            if (!listStudents.stream().anyMatch((s) -> (s.getStudentCode() == null ? student.getStudentCode() == null : s.getStudentCode().equals(student.getStudentCode())))) {
                PreparedStatement preparedStatement = cnn.prepareStatement("INSERT INTO student (id, student_code, ho_ten, gioi_tinh, ngay_sinh, ngay_vao_doan, dien_thoai, dia_chi, chuc_vu, so_doan, sinh_hoat, id_lop, id_khoa_hoc, doan_phi) VALUES  (NULL,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                preparedStatement.setString(1, student.getStudentCode());
                preparedStatement.setString(2, student.getHoTen());
                preparedStatement.setString(3,student.getGioiTinh());
                preparedStatement.setString(4, student.getNgaySinh());
                preparedStatement.setString(5, student.getNgayVaoDoan());
                preparedStatement.setString(6, student.getDienThoai());
                student.setDiaChiImport();
                preparedStatement.setString(7, student.getDiaChiImport());
                preparedStatement.setString(8,student.getChucVu());
                preparedStatement.setBoolean(9, student.isSoDoan());
                preparedStatement.setBoolean(10, student.isSinhHoat());
                preparedStatement.setInt(11, student.getIdLop());
                preparedStatement.setInt(12,student.getIdKhoaHoc());
                preparedStatement.setBoolean(13,student.isDoanPhi());
                if (preparedStatement.executeUpdate() < 0) {
                    return ERROR;
                } else {
                    return SUCCESS;
                }
            } else {
                return DUPLICATE_RESULT;
            }
        } catch (SQLException ex) {
            return ERROR;
        }
    }

    /**
     * Edit student from database
     * @param student
     * @return 
     */
    @Override
    public int update(Student student) {
        try {
            if (listStudents == null) listStudents = this.getAll();
            if (listStudents.stream().anyMatch((s) -> s.getId() == student.getId())) {
                PreparedStatement preparedStatement = cnn.prepareStatement("UPDATE student SET ho_ten = ?, ngay_sinh = ?, ngay_vao_doan = ?, dien_thoai = ?, dia_chi = ?, chuc_vu = ?, so_doan = ?, sinh_hoat = ?, id_lop = ?, id_khoa_hoc = ?, doan_phi = ?, student_code = ? WHERE id = ?");
                preparedStatement.setString(1, student.getHoTen());
                preparedStatement.setString(2, student.getNgaySinh());
                preparedStatement.setString(3, student.getNgayVaoDoan());
                preparedStatement.setString(4, student.getDienThoai());
                student.setDiaChiImport();
                preparedStatement.setString(5, student.getDiaChiImport());
                preparedStatement.setString(6, student.getChucVu());
                preparedStatement.setBoolean(7, student.isSoDoan());
                preparedStatement.setBoolean(8, student.isSinhHoat());
                preparedStatement.setInt(9, student.getIdLop());
                preparedStatement.setInt(10, student.getIdKhoaHoc());
                preparedStatement.setBoolean(11, student.isDoanPhi());
                preparedStatement.setString(12, student.getStudentCode());
                preparedStatement.setInt(13, student.getId());
                if (preparedStatement.executeUpdate() < 0) {
                    return ERROR;
                } else {
                    return SUCCESS;
                }
            } else {
                return DUPLICATE_RESULT;
            }
        } catch (SQLException e) {
            return ERROR;
        }
    }

    @Override
    public int delete(Student student) {
        try {
            PreparedStatement preparedStatement = cnn.prepareStatement("DELETE FROM student WHERE id = ?");
            preparedStatement.setInt(1, student.getId());
            if (preparedStatement.executeUpdate() < 0) {
                return ERROR;
            } else {
                return SUCCESS;
            }
        } catch (SQLException e) {
            return ERROR;
        }
    }
    
    public List<Student> getStudentWithIdLop(int idLop)
    {
        if (listStudents == null) listStudents=this.getAll();
        return listStudents.stream().filter(u -> u.getIdLop() == idLop).collect(Collectors.toList());
    }
    
    public List<Student> getStudentWithIdNganhHoc(int idNganh)
    {
        List<Student> returnList = new LinkedList<>();
        List<Lop> listLop = lopDAO.getLopWithIdNganh(idNganh);
        for (Lop lop : listLop)
        {
            returnList.addAll(this.getStudentWithIdLop(lop.getId()));
        }
        return returnList;
    }
    
    public List<Student> getStudentWithIdKhoa(int idKhoa)
    {
        List<Student> returnList = new LinkedList<>();
        List<NganhHoc> listNganhHoc = nganhHocDAO.getNganhHocWithIdKhoa(idKhoa);
        for (NganhHoc nganhHoc : listNganhHoc)
        {
            returnList.addAll(this.getStudentWithIdNganhHoc(nganhHoc.getId()));
        }
        return returnList;
    }
}
