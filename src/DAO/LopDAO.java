/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;
import Common.DBConnection;
import Model.Lop;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
/**
 *
 * @author linhnd
 */
public class LopDAO implements DAOImplement<Lop>{
    
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private List<Lop> listLop = null;
    
    @Override
    public List<Lop> getAll() {
        listLop = new LinkedList<>();
        try {
            statement = DBConnection.createConnection().createStatement();
            resultSet = statement.executeQuery("SELECT * FROM lop");
            while (resultSet.next()) {
                Lop lop = new Lop();
                lop.setId(resultSet.getInt("id"));
                lop.setMaLop(resultSet.getString("ma_lop"));
                lop.setTenLop(resultSet.getString("ten_lop"));
                lop.setIdNganh(resultSet.getInt("id_nganh"));
                listLop.add(lop);
            }
            resultSet.close();
            statement.close();
            DBConnection.closeConnection();
        } catch (SQLException ex) {
        }
        return listLop;
    }

    @Override
    public Optional<Lop> get(int id) {
        if (listLop == null) {
            listLop = this.getAll();
        }
        return listLop.stream().filter(u -> u.getId() == id).findFirst();
    }

    @Override
    public int save(Lop lop) {
        if (listLop == null) {
            listLop = this.getAll();
        }
        try {
            if (!listLop.stream().anyMatch((s) -> (s.getMaLop()== null ? lop.getMaLop()== null : s.getMaLop().equals(lop.getMaLop())))) {
                preparedStatement = DBConnection.createConnection().prepareStatement("INSERT INTO lop (id, ma_lop, ten_lop, id_nganh) VALUES  (NULL,?,?,?)");
                preparedStatement.setString(1, lop.getMaLop());
                preparedStatement.setString(2, lop.getTenLop());
                preparedStatement.setInt(3, lop.getIdNganh());
                int result = preparedStatement.executeUpdate();
                DBConnection.closeConnection();
                if (result < 0) {
                    return ERROR;
                } else {
                    return SUCCESS;
                }
            } else {
                return DUPLICATE_RESULT;
            }
        } catch (SQLException ex) {
            return ERROR;
        }
        }

    @Override
    public int update(Lop lop) {
        if (listLop == null) {
            listLop = this.getAll();
        }
        try {
            if (!listLop.stream().anyMatch((s) -> s.getId() != lop.getId() && (s.getMaLop()== null ? lop.getMaLop()== null : s.getMaLop().equals(lop.getMaLop())))) {
                preparedStatement = DBConnection.createConnection().prepareStatement("UPDATE lop SET ten_lop = ?, id_nganh = ? WHERE id = ? AND ma_lop = ?");
                preparedStatement.setString(1, lop.getTenLop());
                preparedStatement.setInt(2, lop.getIdNganh());
                preparedStatement.setInt(3, lop.getId());
                preparedStatement.setString(4, lop.getMaLop());
                int result = preparedStatement.executeUpdate();
                DBConnection.closeConnection();
                if (result < 0) {
                    return ERROR;
                } else {
                    return SUCCESS;
                }
            } else {
                return DUPLICATE_RESULT;
            }
        } catch (SQLException e) {
            return ERROR;
        }
    }

    @Override
    public int delete(Lop lop) {
        try {
            preparedStatement = DBConnection.createConnection().prepareStatement("DELETE FROM lop WHERE id = ?");
            preparedStatement.setInt(1, lop.getId());
            int result = preparedStatement.executeUpdate();
            DBConnection.closeConnection();
            if (result < 0) {
                return ERROR;
            } else {
                return SUCCESS;
            }
        } catch (SQLException e) {
            return ERROR;
        }
    }
    
    public List<Lop> getLopWithIdNganh(int idNganh)
    {
        if (listLop==null) listLop=this.getAll();
        return listLop.stream().filter(u -> u.getIdNganh() == idNganh).collect(Collectors.toList());
    }
}
