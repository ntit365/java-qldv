/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Common.DBConnection;
import Model.KhoaHoc;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author truon
 */
public class KhoaHocDAO implements DAOImplement<KhoaHoc>{
    
    private Statement statement = null;
    private ResultSet resultSet = null;
    private PreparedStatement preparedStatement = null;
    private List<KhoaHoc> listKhoaHocs = null;
    
    @Override
    public List<KhoaHoc> getAll() {
        listKhoaHocs = new LinkedList<>();
        try {
            statement = DBConnection.createConnection().createStatement();
            resultSet = statement.executeQuery("SELECT * FROM khoahoc");
            while (resultSet.next()) {
                KhoaHoc khoaHoc = new KhoaHoc();
                khoaHoc.setId(resultSet.getInt("id"));
                khoaHoc.setMaKhoaHoc(resultSet.getString("ma_khoa_hoc"));
                khoaHoc.setTenKhoaHoc(resultSet.getString("ten_khoa_hoc"));
                khoaHoc.setLastCode(resultSet.getInt("lastCode"));
                listKhoaHocs.add(khoaHoc);
            }
            resultSet.close();
            statement.close();
            DBConnection.closeConnection();
        } catch (SQLException ex) {
        }
        return listKhoaHocs;
    }

    @Override
    public Optional<KhoaHoc> get(int id) {
        if (listKhoaHocs == null) {
            listKhoaHocs = this.getAll();
        }
        return listKhoaHocs.stream().filter(s -> s.getId() == id).findFirst();
    }

    @Override
    public int save(KhoaHoc khoaHoc) {
        if (listKhoaHocs == null) {
            listKhoaHocs = this.getAll();
        }
        try {
            if (!listKhoaHocs.stream().anyMatch((s) -> (s.getMaKhoaHoc()== null ? khoaHoc.getMaKhoaHoc()== null : s.getMaKhoaHoc().equals(khoaHoc.getMaKhoaHoc())))) {
                preparedStatement = DBConnection.createConnection().prepareStatement("INSERT INTO khoahoc (id, ma_khoa_hoc, ten_khoa_hoc, lastCode) VALUES  (NULL,?,?,?)");
                preparedStatement.setString(1, khoaHoc.getMaKhoaHoc());
                preparedStatement.setString(2, khoaHoc.getTenKhoaHoc());
                preparedStatement.setInt(3,khoaHoc.getLastCode());
                int result = preparedStatement.executeUpdate();
                DBConnection.closeConnection();
                if (result < 0) {
                    return ERROR;
                } else {
                    return SUCCESS;
                }
            } else {
                return DUPLICATE_RESULT;
            }
        } catch (SQLException ex) {
            return ERROR;
        }
    }

    @Override
    public int update(KhoaHoc khoaHoc) {
        if (listKhoaHocs == null) {
            listKhoaHocs = this.getAll();
        }
        try {
            if (!listKhoaHocs.stream().anyMatch((s) -> s.getId() != khoaHoc.getId() && (s.getMaKhoaHoc()== null ? khoaHoc.getMaKhoaHoc()== null : s.getMaKhoaHoc().equals(khoaHoc.getMaKhoaHoc())))) {
                preparedStatement = DBConnection.createConnection().prepareStatement("UPDATE khoahoc SET ten_khoa_hoc = ?, lastCode = ? WHERE id = ? AND ma_khoa_hoc = ?");
                preparedStatement.setString(1, khoaHoc.getTenKhoaHoc());
                preparedStatement.setInt(2, khoaHoc.getLastCode());
                preparedStatement.setInt(3, khoaHoc.getId());
                preparedStatement.setString(4, khoaHoc.getMaKhoaHoc());
                int result = preparedStatement.executeUpdate();
                DBConnection.closeConnection();
                if (result < 0) {
                    return ERROR;
                } else {
                    return SUCCESS;
                }
            } else {
                return DUPLICATE_RESULT;
            }
        } catch (SQLException e) {
            return ERROR;
        }
    }

    @Override
    public int delete(KhoaHoc khoaHoc) {
        try {
            preparedStatement = DBConnection.createConnection().prepareStatement("DELETE FROM khoahoc WHERE id = ?");
            preparedStatement.setInt(1, khoaHoc.getId());
            int result = preparedStatement.executeUpdate();
            DBConnection.closeConnection();
            if (result < 0) {
                return ERROR;
            } else {
                return SUCCESS;
            }
        } catch (SQLException e) {
            return ERROR;
        }
    }
    
    public int updateLastCode(KhoaHoc khoaHoc)
    {
        if (listKhoaHocs == null) {
            listKhoaHocs = this.getAll();
        }
        try {
            preparedStatement = DBConnection.createConnection().prepareStatement("UPDATE khoahoc SET lastCode = ? WHERE id = ?");
            preparedStatement.setInt(1, khoaHoc.getLastCode());
            preparedStatement.setInt(2, khoaHoc.getId());
            int result = preparedStatement.executeUpdate();
            DBConnection.closeConnection();
            if (result < 0) {
                return ERROR;
            } else {
                return SUCCESS;
            }             
        } catch (SQLException e) {
            return ERROR;
        }
    }
}
