/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.List;
import java.util.Optional;

/**
 *
 * @author truon
 */
public interface DAOImplement<T> {
    final int DUPLICATE_RESULT = 500;
    final int ERROR = 404;
    final int SUCCESS = 200;
    final int FORBIDDEN = 403;
    final int SQL_ERROR = 401;
    List<T> getAll();
    Optional<T> get(int id);
    int save(T t);
    int update(T t);
    int delete(T t);
}
