/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Common.BcryptPassword;
import Common.DBConnection;
import Model.Khoa;
import Model.Permission;
import Model.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import Model.Role;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author truon
 */
public class LoginDAO {
    private static boolean loginState = false;
    private static User userLogin = null;
    
    public static boolean authenticate(User user){
       String userName = user.getUsername();
       String passWord = user.getPassword();

       try {
           Statement statement = DBConnection.createConnection().createStatement();
           ResultSet resultSet = statement.executeQuery("SELECT * FROM users");
           while (resultSet.next()) {
               if (userName.equals(resultSet.getString("username")) && BcryptPassword.checkPassword(passWord, resultSet.getString("password"))) {
                   loginState = true;
                   userLogin = new User();
                   userLogin.setId(resultSet.getInt("id"));
                   userLogin.setIdKhoa(resultSet.getInt("id_khoa"));
                   userLogin.setKhoaName(getKhoaName(resultSet.getInt("id_khoa")));
                   userLogin.setIdRole(resultSet.getInt("id_role"));
                   userLogin.setRoleName(getRoleName(resultSet.getInt("id_role")));
                   userLogin.setName(resultSet.getString("name"));
                   userLogin.setPassword(resultSet.getString("password"));
                   userLogin.setUsername(resultSet.getString("username"));
                   return true;
               }
           }
           DBConnection.closeConnection();
       } catch (SQLException ex) {
       }
       return false;
    }
    
    public static Role getRole(){
        Role role = null;
        try {
            Statement statement = DBConnection.createConnection().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM role where id='"+userLogin.getIdRole()+"'");
            while (resultSet.next()) {
                role = new Role();
                role.setId(Integer.parseInt(resultSet.getString("id")));
                role.setName(resultSet.getString("name"));
                role.setLabel(resultSet.getString("label"));
                role.setPermissons(resultSet.getString("permissions"));
            }
            DBConnection.closeConnection();
        } catch (SQLException ex) {
        }
        return role;
    }
    
    public static boolean hasRoleByName(String roleName){
        Role role = getRole();
        return roleName.equals(role.getName());
    }
    
    public static boolean hasRoleById(int id){
        Role role = getRole();
        return role.getId() == id;
    }
    
    public static List<Permission> getPermission(){
        List<Permission> listPer = new LinkedList<>();
        try {
            Statement statement = DBConnection.createConnection().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM permission WHERE id IN " + getRole().getPermissons());
            while (resultSet.next()) {
                Permission per = new Permission();
                per.setId(Integer.parseInt(resultSet.getString("id")));
                per.setLabel(resultSet.getString("label"));
                per.setName(resultSet.getString("name"));
                listPer.add(per);
            }
            DBConnection.closeConnection();
        } catch (SQLException e) {
        }
        return listPer;
    }
    
    public static boolean hasPermissionByName(String perName){
        if (hasRoleByName("admin")) {
            return true;
        }
        else {
        List<Permission> listPer = getPermission();
        return listPer.stream().anyMatch((s) -> s.getName().equalsIgnoreCase(perName));
        }
    }
    
    public static boolean hasPermissionByID(int id){
        if (hasRoleByName("admin")) {
            return true;
        }
        else {
            List<Permission> listPer = getPermission();
            return listPer.stream().anyMatch((s) -> s.getId() == id);
        }  
    }
    
    public static String getKhoaName(int id)
    {
        KhoaDAO khoaDAO = new KhoaDAO();
        Optional<Khoa> khoa = khoaDAO.get(id);
        return khoa.get().getTenKhoa();
    }
    
    public static String getRoleName(int id)
    {
        RoleDAO roleDAO = new RoleDAO();
        Optional<Role> role = roleDAO.get(id);
        return role.get().getLabel();
    }
    
    public static boolean loginStatus(){
        return loginState;
    }
    
    public static User getUserLogin(){
        return userLogin;
    }
    
    public static void logout(){
        loginState = false;
        userLogin = null;
    }
    
}
