/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import static Common.DialogHelper.DIALOG_HELPER;
import DAO.LoginDAO;
import DAO.RoleDAO;
import Model.Role;
import Model.User;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author linhnd
 */
public class DoanVienController implements Initializable {

    @FXML
    private Button btnTimKiemDoanVien;
    @FXML
    private Button btnQuanLyDoanVien;
    @FXML
    private Button btnDoanPhi;
    @FXML
    private Button btnChuyenSinhHoat;
    @FXML
    private AnchorPane MainViewPane;
    @FXML
    private Label txtRole;
    @FXML
    private Label txtNameLogin;
    @FXML
    private ImageView btnBack1;
    @FXML
    private ImageView btnLogout1;
    
    public final int TKDV = 1;
    public final int QLDV = 2;
    public final int DOANPHI = 3;
    public final int CHUYENSH = 4;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //TODO
    }

    @FXML
    private void btnTimKiemDoanVienClicked(MouseEvent event) throws IOException {
        if (LoginDAO.hasPermissionByName("view_doanvien")) {
            loadPane(TKDV);
            btnTimKiemDoanVien.getStyleClass().add("item-menu-active"); 
            btnDoanPhi.getStyleClass().remove("item-menu-active"); 
            btnChuyenSinhHoat.getStyleClass().remove("item-menu-active"); 
            btnQuanLyDoanVien.getStyleClass().remove("item-menu-active");
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
        
    }

    @FXML
    private void btnQuanLyDoanVienClicked(MouseEvent event) throws IOException {
        if (LoginDAO.hasPermissionByName("view_doanvien")) {
            loadPane(QLDV);
            btnTimKiemDoanVien.getStyleClass().remove("item-menu-active"); 
            btnDoanPhi.getStyleClass().remove("item-menu-active"); 
            btnChuyenSinhHoat.getStyleClass().remove("item-menu-active"); 
            btnQuanLyDoanVien.getStyleClass().add("item-menu-active");
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
        
    }

    @FXML
    private void btnDoanPhiClicked(MouseEvent event) throws IOException {
        if (LoginDAO.hasPermissionByName("view_doanphi")) {
            loadPane(DOANPHI);
            btnTimKiemDoanVien.getStyleClass().remove("item-menu-active"); 
            btnDoanPhi.getStyleClass().add("item-menu-active"); 
            btnChuyenSinhHoat.getStyleClass().remove("item-menu-active"); 
            btnQuanLyDoanVien.getStyleClass().remove("item-menu-active");
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
        
    }

    @FXML
    private void btnChuyenSinhHoat(MouseEvent event) throws IOException {
        if (LoginDAO.hasPermissionByName("view_chuyensh")) {
            loadPane(CHUYENSH);
            btnTimKiemDoanVien.getStyleClass().remove("item-menu-active"); 
            btnDoanPhi.getStyleClass().remove("item-menu-active"); 
            btnChuyenSinhHoat.getStyleClass().add("item-menu-active"); 
            btnQuanLyDoanVien.getStyleClass().remove("item-menu-active");
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
        
    }

    @FXML
    private void btnBackClicked(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/View/MainMenu.fxml"));
        Scene scene = new Scene(root);
        Controller.window.setScene(scene);
    }

    @FXML
    private void btnLogoutClicked(MouseEvent event) throws IOException {
        LoginDAO.logout();
        Parent root = FXMLLoader.load(getClass().getResource("/View/Login.fxml"));
        Scene scene = new Scene(root);
        Controller.window.setScene(scene);
    }
    
    public void loadPaneStart(boolean is_doanphi, boolean is_chuyen_sh) {
        try {
            if (!is_doanphi && !is_chuyen_sh && LoginDAO.hasPermissionByName("view_doanvien")) {
               loadPane(TKDV);
               btnTimKiemDoanVien.getStyleClass().add("item-menu-active"); 
            } else if (is_doanphi && !is_chuyen_sh && LoginDAO.hasPermissionByName("view_doanphi")) {
               loadPane(DOANPHI);
               btnDoanPhi.getStyleClass().add("item-menu-active"); 
            } else if (!is_doanphi && is_chuyen_sh && LoginDAO.hasPermissionByName("view_chuyensh")) {
               loadPane(CHUYENSH);
               btnChuyenSinhHoat.getStyleClass().add("item-menu-active"); 
            }
        } catch (IOException ex) {
            Logger.getLogger(DoanVienController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void loadPane(int type) throws IOException {
        switch(type) {
            case 1:{
                AnchorPane loadPane = FXMLLoader.load(getClass().getResource("/View/DoanVien/TimKiemDoanVienUI.fxml"));
                MainViewPane.getChildren().setAll(loadPane);
                break;
            }
            case 2:{
                AnchorPane loadPane = FXMLLoader.load(getClass().getResource("/View/DoanVien/QLDoanVienUI.fxml"));
                MainViewPane.getChildren().setAll(loadPane);
                break;
            }
            case 3:{
                 AnchorPane loadPane = FXMLLoader.load(getClass().getResource("/View/DoanVien/DoanFeeUI.fxml"));
                MainViewPane.getChildren().setAll(loadPane);
                break;
            }
            case 4:{
               AnchorPane loadPane = FXMLLoader.load(getClass().getResource("/View/DoanVien/ChuyenSinhHoatUI.fxml"));
                MainViewPane.getChildren().setAll(loadPane);
                break;
            }
        }
    }

    private void loadUserLogin() {
        User user = LoginDAO.getUserLogin();
        txtNameLogin.setText(user.getName());
        RoleDAO roleDAO = new RoleDAO();
        Optional<Role> role = roleDAO.get(user.getIdRole());
        txtRole.setText(role.get().getLabel());
    }
}
