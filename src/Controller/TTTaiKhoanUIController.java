/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Common.BcryptPassword;
import static Common.DialogHelper.DIALOG_HELPER;
import Common.Validate;
import static DAO.DAOImplement.SUCCESS;
import DAO.KhoaDAO;
import DAO.LoginDAO;
import DAO.RoleDAO;
import DAO.UserDAO;
import Model.Khoa;
import Model.Role;
import Model.User;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 *
 * @author truon
 */
public class TTTaiKhoanUIController implements Initializable {

    @FXML
    private TextField txtUsername;
    @FXML
    private TextField txtName;
    @FXML
    private PasswordField txtPassword;
    @FXML
    private PasswordField txtRepassword;
    @FXML
    private ComboBox<Khoa> cboDonVi;
    @FXML
    private Label lblUsername;
    @FXML
    private Label lblName;
    @FXML
    private Label lblDonVi;
    @FXML
    private Label lblRole;
    @FXML
    private ComboBox<Role> cboVaiTro;
    @FXML
    private Button btnCapNhat;
    
    private User user = null;
    @FXML
    private Label lblErrorPassword;
    @FXML
    private Label lblErrorName;
    @FXML
    private Label lblErrorRepassword;
    
    private boolean status = false;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if (user == null) {
            user = LoginDAO.getUserLogin();
        }
        loadInfo();
        loadCboDonVi();
        loadCboVaiTro();
        checkPassword();
        clearData();
    }    
    
    private void loadInfo() {
        lblUsername.setText(user.getUsername());
        lblName.setText(user.getName());
        lblRole.setText(user.getRoleName());
        lblDonVi.setText(user.getKhoaName());
        txtUsername.setDisable(true);
        txtUsername.setText(user.getUsername());
        txtName.setText(user.getName());
        KhoaDAO khoaDAO = new KhoaDAO();
        Optional<Khoa> khoa = khoaDAO.get(user.getIdKhoa());
        cboDonVi.getSelectionModel().select(khoa.get());
        RoleDAO roleDAO = new RoleDAO();
        Optional<Role> role = roleDAO.get(user.getIdRole());
        cboVaiTro.getSelectionModel().select(role.get());
        cboDonVi.setDisable(true);
        cboVaiTro.setDisable(true);
    }

    @FXML
    private void btnCapNhatClicked(ActionEvent event) {
        boolean isName = Validate.requiredTextField(txtName, lblErrorName, "Họ tên không được để trống");
        boolean isPassword = Validate.requiredTextField(txtPassword, lblErrorPassword, "Mật khẩu không được để trống!");
        boolean isRepassword = Validate.requiredTextField(txtRepassword, lblErrorRepassword, "Mật khẩu không được để trống!");
        if (isPassword && isRepassword) {
            validatePassword("Mật khẩu không trùng khớp!");
        }
        if (isName && isPassword && isRepassword && this.status) {
            user.setName(txtName.getText());
            user.setPassword(BcryptPassword.hashPassword(txtPassword.getText()));
            UserDAO userDAO = new UserDAO();
            int result = userDAO.updateInfo(user);
            if (result == SUCCESS) {
                DIALOG_HELPER.infoDialog("Thông báo", "Cập nhật thông tin thành công");
                loadInfo();
                txtPassword.clear();
                txtRepassword.clear();
                clearData();
            } else {
                DIALOG_HELPER.infoDialog("Lỗi", "Có vấn đề trong quá trình cập nhật tài khoản! Vui lòng thử lại!");
            }
        }   
    }
    
    public void loadCboVaiTro() {
        RoleDAO roleDAO = new RoleDAO();
        List<Role> listRoles = roleDAO.getAll();
        ObservableList<Role> listData = FXCollections.observableList(listRoles);
        cboVaiTro.setItems(listData);
        cboVaiTro.setConverter(new StringConverter<Role>() {

            @Override
            public String toString(Role object) {
                return object.getLabel();
            }

            @Override
            public Role fromString(String string) {
                return cboVaiTro.getItems().stream().filter(ap -> 
                    ap.getLabel().equals(string)).findFirst().orElse(null);
            }
        });
        
    }
    
    public void loadCboDonVi() {
        KhoaDAO khoaDAO = new KhoaDAO();
        List<Khoa> listKhoas = khoaDAO.getAll();
        ObservableList<Khoa> listData = FXCollections.observableList(listKhoas);
        cboDonVi.setItems(listData);
        cboDonVi.setConverter(new StringConverter<Khoa>() {

            @Override
            public String toString(Khoa khoa) {
                return khoa.getTenKhoa();
            }

            @Override
            public Khoa fromString(String string) {
                return cboDonVi.getItems().stream().filter(ap -> 
                    ap.getTenKhoa().equals(string)).findFirst().orElse(null);
            }
        });
    }
    
    private void clearData() {
        lblErrorName.setText(null);
        lblErrorPassword.setText(null);
        lblErrorRepassword.setText(null);
    }
    
    private void checkPassword() {
        txtRepassword.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            String password = txtPassword.getText();
            this.status = newValue.equals(password);
        });
        txtPassword.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            String repassword = txtRepassword.getText();
            this.status = newValue.equals(repassword);
        });
    }
    
    private void validatePassword(String errorMessage) {
        String message = null;
        if (!this.status) {
            message = errorMessage;
        }
        lblErrorPassword.setText(message);
        lblErrorRepassword.setText(message);
        lblErrorPassword.setTextFill(Color.web("#bc3216"));
        lblErrorRepassword.setTextFill(Color.web("#bc3216"));
    }
}
