/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import static Common.DialogHelper.DIALOG_HELPER;
import Common.TableHelper;
import Common.Validate;
import static DAO.DAOImplement.DUPLICATE_RESULT;
import static DAO.DAOImplement.SUCCESS;
import DAO.KhoaDAO;
import DAO.LoginDAO;
import DAO.LopDAO;
import DAO.NganhHocDAO;
import Model.Khoa;
import Model.Lop;
import Model.NganhHoc;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 *
 * @author Hoang Trang
 */
public class QLLopController implements Initializable {

    @FXML
    private JFXButton btnClear;
    @FXML
    private Label lblErrorMa;
    @FXML
    private Label lblErrorTen;
    @FXML
    private Label lblErrorKhoa;
    @FXML
    private Label lblErrorNganh;
    @FXML
    private TableView<CustomLop> tableLop;
    @FXML
    private JFXButton btnThem;
    @FXML
    private JFXButton btnCapNhat;
    @FXML
    private JFXButton btnXoa;
    @FXML
    private TextField txtMaLop;
    @FXML
    private TextField txtTenLop;
    @FXML
    private ComboBox<NganhHoc> cboNganh;
    @FXML
    private ComboBox<Khoa> cboKhoa;
    
    private List<Lop> listLop;
    private List<CustomLop> listCustomLop;
    private LopDAO  lopDAO = null;
    private NganhHocDAO nganhDAO = null;
    private KhoaDAO khoaDAO = null;
    private Lop lop = null;
    private CustomLop customLop = null;
    
   
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lopDAO =new LopDAO();
        khoaDAO =new KhoaDAO();
        nganhDAO =new NganhHocDAO();
        loadTable();
        loadCboKhoa();
        clearData();
    }   
    
    @FXML
    private void btnThemClicked(MouseEvent event) {
        if (LoginDAO.hasPermissionByName("add_lop")) {
            if (lop != null) {
                clearData();
                DIALOG_HELPER.infoDialog("Lỗi", "Lớp đã tồn tại!");
            } else {
                boolean isKhoa = Validate.requiredCombobox(cboKhoa, lblErrorKhoa, "Vui lòng chọn khoa");
                boolean isNganh = Validate.requiredCombobox(cboNganh, lblErrorNganh, "Vui lòng chọn ngành học");
                boolean isMa = Validate.requiredTextField(txtMaLop, lblErrorMa, "Mã lớp không được để trống");
                boolean isTen = Validate.requiredTextField(txtTenLop, lblErrorTen, "Tên lớp không được để trống");
                if (isTen && isKhoa && isMa && isNganh) {
                    lop = new Lop();
                    lop.setIdNganh(cboNganh.getSelectionModel().getSelectedItem().getId());
                    lop.setMaLop(txtMaLop.getText());
                    lop.setTenLop(txtTenLop.getText());
                    int status = lopDAO.save(lop);
                    switch (status) {
                        case SUCCESS : {
                            clearData();
                            reloadTable();
                            DIALOG_HELPER.infoDialog("Thông báo", "Thêm lớp thành công");
                            break;
                        }
                        case DUPLICATE_RESULT: {
                            clearData();
                            DIALOG_HELPER.infoDialog("Lỗi", "Lớp đã tồn tại");
                            break;
                        }
                        default:
                            DIALOG_HELPER.infoDialog("Lỗi", "Đã xảy ra lỗi trong quá trình thêm!");
                            break;
                    }
                }
            }
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
    }

    @FXML
    private void btnCapNhatClicked(MouseEvent event) {
        if (LoginDAO.hasPermissionByName("update_lop")) {
            if (lop != null) {
                boolean isTen = Validate.requiredTextField(txtTenLop, lblErrorTen, "Tên lớp không được để trống");
                boolean isNganh = Validate.requiredCombobox(cboNganh, lblErrorNganh, "Vui lòng chọn ngành học");
                if (isNganh && isTen) {
                    lop.setTenLop(txtTenLop.getText());
                    lop.setIdNganh(cboNganh.getSelectionModel().getSelectedItem().getId());
                    int status = lopDAO.update(lop);
                    if (status == SUCCESS) {
                        clearData();
                        reloadTable();
                        DIALOG_HELPER.infoDialog("Thông báo", "Cập nhật lớp thành công!");
                    }
                }
            }
            else{
                DIALOG_HELPER.infoDialog("Lỗi", "Vui lòng chọn lớp cần cập nhật!");
            }
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
    }

    @FXML
    private void btnXoaClicked(MouseEvent event) {
        if (LoginDAO.hasPermissionByName("delete_lop")) {
            if (lop != null) {
                Optional<ButtonType> resultDialog = DIALOG_HELPER.dialogConfirm("Xác nhận", "Bạn có muốn xóa lớp #ID: " + lop.getId() + " hay không?");
                if (DIALOG_HELPER.confirmAction(resultDialog)) {
                    int status=lopDAO.delete(lop);
                    if(status==SUCCESS) 
                    {
                        reloadTable();
                        clearData();
                        DIALOG_HELPER.infoDialog("Thông báo", "Xóa khóa học thành công!");
                    }
                }
            } else {
                DIALOG_HELPER.infoDialog("Lỗi", "Vui lòng chọn lớp cần xóa!");
            }
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
    }

    @FXML
    private void tableClickLop(MouseEvent event) {
        if (tableLop.getSelectionModel().getSelectedItem() != null) {
            lop = tableLop.getSelectionModel().getSelectedItem();
            txtMaLop.setText(lop.getMaLop());
            txtTenLop.setText(lop.getTenLop());
            NganhHoc nganh=nganhDAO.get(lop.getIdNganh()).get();
            Khoa khoa =khoaDAO.get(nganh.getIdKhoa()).get();
            cboKhoa.getSelectionModel().select(khoa);
            loadCboNganh(nganh.getId());
            cboNganh.getSelectionModel().select(nganh);
            txtMaLop.setDisable(true);
        }
    }
    
    @FXML
    private void cboKhoaOnAction(ActionEvent event) {
        if (cboKhoa.getSelectionModel().getSelectedItem() != null) {
            loadCboNganh(cboKhoa.getSelectionModel().getSelectedItem().getId());
            cboNganh.getSelectionModel().select(null);
        } 
    }
    
    @FXML
    private void btnClearClicked(MouseEvent event) {
        clearData();
    }
    
    private void loadCboKhoa() {
        List<Khoa> listKhoas = khoaDAO.getAll();
        ObservableList<Khoa> listData = FXCollections.observableList(listKhoas);
        cboKhoa.setItems(listData);
        cboKhoa.setConverter(new StringConverter<Khoa>() {

            @Override
            public String toString(Khoa khoa) {
                return khoa.getTenKhoa();
            }

            @Override
            public Khoa fromString(String string) {
                return cboKhoa.getItems().stream().filter(ap -> 
                    ap.getTenKhoa().equals(string)).findFirst().orElse(null);
            }
        });
    }
    
    private void loadCboNganh(int idKhoa) {
        List<NganhHoc> list = nganhDAO.getNganhHocWithIdKhoa(idKhoa);
        ObservableList<NganhHoc> listData= FXCollections.observableArrayList(list);
        cboNganh.setItems(listData);
        cboNganh.setConverter(new StringConverter<NganhHoc>() {
            @Override
            public String toString(NganhHoc nganhhoc) {
                return nganhhoc.getTenNganhHoc();
            }

            @Override
            public NganhHoc fromString(String string) {
                return cboNganh.getItems().stream().filter(ap -> ap.getTenNganhHoc().equals(string)).findFirst().orElse(null);
            }
        });
    }
    
    private void loadTable() {
        String[] titles = {"ID", "Mã lớp", "Tên lớp","Tên ngành"};
        String[] elements = {"id", "maLop", "tenLop", "tenNganh"};
        listLop = lopDAO.getAll();
        listCustomLop = new LinkedList<>();
        Iterator<Lop> itLop = listLop.iterator();
        while (itLop.hasNext()) {
            customLop = new CustomLop(itLop.next());
            customLop.setTenNganh();
            listCustomLop.add(customLop);
        }
        TableHelper<CustomLop> tableHelper = new TableHelper<>(tableLop, titles, elements, listCustomLop);
        tableHelper.setModel();
    }
     
    private void reloadTable(){
        tableLop.getItems().removeAll();
        listLop = lopDAO.getAll();
        listCustomLop = new LinkedList<>();
        Iterator<Lop> itLop = listLop.iterator();
        while (itLop.hasNext()) {
            customLop = new CustomLop(itLop.next());
            customLop.setTenNganh();
            listCustomLop.add(customLop);
        }
        ObservableList<CustomLop> listData = FXCollections.observableList(listCustomLop);
        tableLop.setItems(listData);
    }


    private void clearData() {
        lblErrorKhoa.setText(null);
        lblErrorMa.setText(null);
        lblErrorNganh.setText(null);
        lblErrorTen.setText(null);
        txtMaLop.clear();
        txtTenLop.clear();
        txtMaLop.setDisable(false);
        cboKhoa.getSelectionModel().select(null);
        cboNganh.getSelectionModel().select(null);
        cboNganh.getItems().clear();
        customLop = null;
        lop = null;
        txtMaLop.requestFocus();
    }
    
    public class CustomLop extends Lop
    {
        private String tenNganh;
        
        public CustomLop(Lop lop) {
            super.setId(lop.getId());
            super.setIdNganh(lop.getIdNganh());
            super.setMaLop(lop.getMaLop());
            super.setTenLop(lop.getTenLop());
        }
        
        public String getTenNganh() {
            return tenNganh;
        }
        
        public void setTenNganh()
        {
            nganhDAO = new NganhHocDAO();
            this.tenNganh = nganhDAO.get(super.getIdNganh()).get().getTenNganhHoc();
        }
    }
}
