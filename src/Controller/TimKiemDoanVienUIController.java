/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Common.TableHelper;
import DAO.KhoaDAO;
import DAO.KhoaHocDAO;
import DAO.LopDAO;
import DAO.NganhHocDAO;
import DAO.StudentDAO;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableView;
import Model.*;
import com.jfoenix.controls.JFXButton;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.*;

/**
 * FXML Controller class
 *
 * @author linhnd
 */
public class TimKiemDoanVienUIController implements Initializable {


    
    List <Student> listStudent = null;
    List <CustomStudent> listCustomStudent = null;
    StudentDAO studentDAO = new StudentDAO();
    KhoaHocDAO khoaHocDAO = new KhoaHocDAO();
    KhoaDAO khoaDAO = new KhoaDAO();
    NganhHocDAO nganhHocDAO = new NganhHocDAO();
    LopDAO lopDAO = new LopDAO();
    List<Lop> listLop = null;
    List<KhoaHoc> listKhoaHoc=null;
    List<Khoa> listKhoa = null;
    List<NganhHoc> listNganh = null;
    TableHelper tableHelper = null;
    final String title[] = {"Mã sinh viên", "Họ tên", "Khoá", "Khoa", "Ngành", "Lớp"};
    final String element[] = {"studentCode", "hoTen", "tenKhoaHoc", "tenKhoa", "tenNganh", "tenLop"};
    
    @FXML
    private ComboBox<Khoa> cbKhoa;
    @FXML
    private ComboBox<NganhHoc> cbNganh;
    @FXML
    private ComboBox<Lop> cbLop;
    @FXML
    private ComboBox<KhoaHoc> cbKhoaHoc;
    @FXML
    private JFXTextField txtTimKiem;
    @FXML
    private TableView<CustomStudent> tableDoanVien;
    @FXML
    private JFXButton btnClearAll;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        listKhoa = khoaDAO.getAll();
        listKhoaHoc = khoaHocDAO.getAll();
        listNganh = nganhHocDAO.getAll();
        listLop = lopDAO.getAll();
        loadComboBox();
        loadTable();
    }    

    public void loadTable()
    {
        listStudent = studentDAO.getAll();
        listCustomStudent = new LinkedList<>();
        for (Student student : listStudent)
        {
            CustomStudent ncs = new CustomStudent(student);
            ncs.genKhoaNganhLop();
            listCustomStudent.add(ncs);
        }
        tableHelper = new TableHelper(tableDoanVien,title,element,listCustomStudent);
        tableHelper.setModel();
    }
    
    public void reloadTable()
    {
        tableDoanVien.getItems().removeAll();
        listCustomStudent = new LinkedList<>();
        listStudent = studentDAO.getAll();
        for (Student student : listStudent)
        {
            CustomStudent ncs = new CustomStudent(student);
            ncs.genKhoaNganhLop();
            listCustomStudent.add(ncs);
        }
        if (cbKhoa.getSelectionModel().getSelectedItem()!=null)
            listCustomStudent = listCustomStudent.stream().filter(u -> u.getKhoa().getId() == cbKhoa.getSelectionModel().getSelectedItem().getId()).collect(Collectors.toList());
        if (cbNganh.getSelectionModel().getSelectedItem()!=null)
            listCustomStudent = listCustomStudent.stream().filter(u -> u.getNganhHoc().getId() == cbNganh.getSelectionModel().getSelectedItem().getId()).collect(Collectors.toList());
        if (cbLop.getSelectionModel().getSelectedItem()!=null)
            listCustomStudent = listCustomStudent.stream().filter(u -> u.getLop().getId() == cbLop.getSelectionModel().getSelectedItem().getId()).collect(Collectors.toList());
        if (cbKhoaHoc.getSelectionModel().getSelectedItem()!=null)
            listCustomStudent = listCustomStudent.stream().filter(u -> u.getKhoaHoc().getId() == cbKhoaHoc.getSelectionModel().getSelectedItem().getId()).collect(Collectors.toList());
        tableDoanVien.setItems(FXCollections.observableList(listCustomStudent));
    }
    
    private void LoadcbKhoaHoc()
    {
        ObservableList<KhoaHoc> listData = FXCollections.observableList(listKhoaHoc);
        cbKhoaHoc.setItems(listData);
        cbKhoaHoc.setConverter(new StringConverter<KhoaHoc>() {
            @Override
            public String toString(KhoaHoc khoaHoc) {
                return khoaHoc.getTenKhoaHoc();
            }
            @Override
            public KhoaHoc fromString(String string) {
                return cbKhoaHoc.getItems().stream().filter(ap -> 
                    ap.getTenKhoaHoc().equals(string)).findFirst().orElse(null);
            }
        });
    }
    private void LoadcbKhoa()
    {
        ObservableList<Khoa> listData = FXCollections.observableList(listKhoa);
        cbKhoa.setItems(listData);
        cbKhoa.setConverter(new StringConverter<Khoa>() {
            @Override
            public String toString(Khoa khoa) {
                return khoa.getTenKhoa();
            }
            @Override
            public Khoa fromString(String string) {
                return cbKhoa.getItems().stream().filter(ap -> 
                    ap.getTenKhoa().equals(string)).findFirst().orElse(null);
            }
        });
    }
    private void LoadcbNganh(int idKhoa)
    {
        List<NganhHoc> newListNganh = new LinkedList<>();
        newListNganh = nganhHocDAO.getNganhHocWithIdKhoa(idKhoa);
        ObservableList<NganhHoc> listData = FXCollections.observableList(newListNganh);
        cbNganh.setItems(listData);
        cbNganh.setConverter(new StringConverter<NganhHoc>() {
            @Override
            public String toString(NganhHoc nganh) {
                return nganh.getTenNganhHoc();
            }
            @Override
            public NganhHoc fromString(String string) {
                return cbNganh.getItems().stream().filter(ap -> 
                    ap.getTenNganhHoc().equals(string)).findFirst().orElse(null);
            }
        });
    }
    private void LoadcbLop(int idNganh)
    {
        List <Lop> newListLop = new LinkedList<>();
        newListLop = lopDAO.getLopWithIdNganh(idNganh);
        ObservableList<Lop> listData = FXCollections.observableList(newListLop);
        cbLop.setItems(listData);
        cbLop.setConverter(new StringConverter<Lop>() {

            @Override
            public String toString(Lop lop) {
                return lop.getTenLop();
            }

            @Override
            public Lop fromString(String string) {
                return cbLop.getItems().stream().filter(ap -> 
                    ap.getTenLop().equals(string)).findFirst().orElse(null);
            }
        });
    }
    private void loadComboBox()
    {
        LoadcbKhoaHoc();
        LoadcbKhoa();
    }

    @FXML
    private void cbKhoaOnAction(ActionEvent event) {
        if (cbKhoa.getSelectionModel().getSelectedItem()==null) return ;
        LoadcbNganh(cbKhoa.getSelectionModel().getSelectedItem().getId());
        cbNganh.getSelectionModel().select(null);
        cbLop.getSelectionModel().select(null);
        reloadTable();
    }

    @FXML
    private void cbNganhOnAction(ActionEvent event) {
        if (cbNganh.getSelectionModel().getSelectedItem()==null) return ;
        LoadcbLop(cbNganh.getSelectionModel().getSelectedItem().getId());
        cbLop.getSelectionModel().select(null);
        reloadTable();
    }

    @FXML
    private void cbLopOnAction(ActionEvent event) {
        reloadTable();
    }

    @FXML
    private void cbKhoaHocOnAction(ActionEvent event) {
        reloadTable();
    }

    @FXML
    private void btnClearAllClicked(MouseEvent event) {
        if (cbKhoa.getSelectionModel().getSelectedItem()!=null) cbKhoa.getSelectionModel().clearSelection();
        if (cbKhoaHoc.getSelectionModel().getSelectedItem()!=null) cbKhoaHoc.getSelectionModel().clearSelection();
        if (cbNganh.getSelectionModel().getSelectedItem()!=null) cbNganh.getSelectionModel().clearSelection();
        if (cbLop.getSelectionModel().getSelectedItem()!=null) cbLop.getSelectionModel().clearSelection();
        reloadTable();
    }
    
    
    @FXML
    private void txtTimkiemOnKeyReleased(KeyEvent event) {
        List <CustomStudent> listFiltered = new LinkedList<>();
        listFiltered = listCustomStudent.stream().filter(u -> (u.getStudentCode().contains(txtTimKiem.getText()) || u.getHoTen().toUpperCase().contains(txtTimKiem.getText().toUpperCase()))).collect(Collectors.toList());
        tableDoanVien.setItems(FXCollections.observableList(listFiltered));
    }
    
    public class CustomStudent extends Student
    {
        private Lop lop ;
        private NganhHoc nganhHoc ;
        private Khoa khoa ;
        private String tenLop ;
        private String tenNganh ;
        private String tenKhoa ;
        private KhoaHoc khoaHoc ;
        private String tenKhoaHoc;
        public CustomStudent(Student a)
        {
            super.setChucVu(a.getChucVu());
            super.setDiaChi(a.getDiaChi());
            super.setDienThoai(a.getDienThoai());
            super.setGioiTinh(a.getGioiTinh());
            super.setHoTen(a.getHoTen());
            super.setId(a.getId());
            super.setIdDistrict(a.getIdDistrict());
            super.setIdKhoaHoc(a.getIdKhoaHoc());
            super.setIdLop(a.getIdLop());
            super.setIdProvince(a.getIdProvince());
            super.setIdWard(a.getIdWard());
            super.setNgaySinh(a.getNgaySinh());
            super.setNgayVaoDoan(a.getNgayVaoDoan());
            super.setSinhHoat(a.isSinhHoat());
            super.setSoDoan(a.isSoDoan());
            super.setStudentCode(a.getStudentCode());
            super.setDiaChiImport();
        }
        public void genKhoaNganhLop()
        {
            lop = lopDAO.get(super.getIdLop()).get();
            nganhHoc = nganhHocDAO.get(lop.getIdNganh()).get();
            khoa = khoaDAO.get(nganhHoc.getIdKhoa()).get();
            khoaHoc = khoaHocDAO.get(super.getIdKhoaHoc()).get();
            tenKhoa = khoa.getTenKhoa();
            tenKhoaHoc = khoaHoc.getTenKhoaHoc();
            tenNganh = nganhHoc.getTenNganhHoc();
            tenLop = lop.getTenLop();
        }
        public String getTenLop() {
            return tenLop;
        }
        public String getTenNganh() {
            return tenNganh;
        }
        public String getTenKhoa() {
            return tenKhoa;
        }
        public String getTenKhoaHoc() {
            return tenKhoaHoc;
        }
        public Khoa getKhoa(){
            return khoa;
        }
        public KhoaHoc getKhoaHoc(){
            return khoaHoc;
        }
        public NganhHoc getNganhHoc(){
            return nganhHoc;
        }
        public Lop getLop(){
            return lop;
        }
    }

}
