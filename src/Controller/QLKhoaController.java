/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import static Common.DialogHelper.DIALOG_HELPER;
import Common.TableHelper;
import Common.Validate;
import static DAO.DAOImplement.DUPLICATE_RESULT;
import static DAO.DAOImplement.SUCCESS;
import DAO.KhoaDAO;
import DAO.LoginDAO;
import Model.Khoa;
import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author truon
 */
public class QLKhoaController implements Initializable {

    @FXML
    private TextField txtMaKhoa;
    @FXML
    private JFXButton btnThem;
    @FXML
    private JFXButton btnCapNhat;
    @FXML
    private JFXButton btnXoa;
    @FXML
    private TableView<Khoa> tableKhoa;
    @FXML
    private TextField txtTenKhoa;
    @FXML
    private Label lblErrorMa;
    @FXML
    private Label lblErrorTen;
    @FXML
    private JFXButton btnClear;
    
    private KhoaDAO khoaDAO = null;
    private Khoa khoa = null;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        khoaDAO = new KhoaDAO();
        loadTable();
        clearData();
    }

    @FXML
    private void btnThemClicked(MouseEvent event) throws IOException {
        if (LoginDAO.hasPermissionByName("add_khoa")) {
            if (khoa != null) {
                khoa = null;
                clearData();
                DIALOG_HELPER.infoDialog("Lỗi", "Khoa đã tồn tại!");
            } else {
                boolean isMaKhoa = Validate.requiredTextField(txtMaKhoa, lblErrorMa, "Mã khoa không được để trống!");
                boolean isTenKhoa = Validate.requiredTextField(txtTenKhoa, lblErrorTen, "Tên khoa không được để trống!");
                if (isTenKhoa && isMaKhoa) {
                     khoa = new Khoa();
                    khoa.setMaKhoa(txtMaKhoa.getText());
                    khoa.setTenKhoa(txtTenKhoa.getText());
                    int status = khoaDAO.save(khoa);
                    switch (status) {
                        case SUCCESS : {
                            DIALOG_HELPER.infoDialog("Thông báo", "Thêm khoa thành công!");
                            reloadTable();
                            clearData();
                            khoa = null;
                            break;
                        }
                        case DUPLICATE_RESULT : {
                            DIALOG_HELPER.infoDialog("Lỗi", "Khoa đã tồn tại!");
                            break;
                        }
                        default:
                            DIALOG_HELPER.infoDialog("Lỗi", "Đã xảy ra lỗi trong quá trình thêm!");
                    }
                }
            }
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
    }

    @FXML
    private void btnCapNhatClicked(MouseEvent event) {
        if (LoginDAO.hasPermissionByName("update_khoa")) {
            if (khoa != null) {
                boolean isTenKhoa = Validate.requiredTextField(txtTenKhoa, lblErrorTen, "Tên khoa không được để trống!");
                if (isTenKhoa) {
                    khoa.setTenKhoa(txtTenKhoa.getText());
                    int status = khoaDAO.update(khoa);
                    if (status == SUCCESS) {
                        reloadTable();
                        DIALOG_HELPER.infoDialog("Thông báo", "Cập nhật khoa thành công!");
                        clearData();
                    }
                }
            } else {
                DIALOG_HELPER.infoDialog("Lỗi", "Vui lòng chọn khoa cần cập nhật!");
            }
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
    }

    @FXML
    private void btnXoaClicked(MouseEvent event) {
        if (LoginDAO.hasPermissionByName("delete_khoa")) {
            if (khoa != null) {
                Optional<ButtonType> resultDialog = DIALOG_HELPER.dialogConfirm("Xác nhận", "Bạn có muốn xóa tài khoản #ID: " + khoa.getId() + " hay không?");
                if (DIALOG_HELPER.confirmAction(resultDialog)) {
                    int stastus = khoaDAO.delete(khoa);
                    if (stastus == SUCCESS) {
                        reloadTable();
                        clearData();
                        DIALOG_HELPER.infoDialog("Thông báo", "Xóa khoa thành công!");
                    }
                }
            } else {
                DIALOG_HELPER.infoDialog("Lỗi", "Vui lòng chọn khoa cần xóa!");
            }
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
    }
    
    @FXML
    private void tableKhoaClicked(MouseEvent event) {
        if (tableKhoa.getSelectionModel().getSelectedItem() != null) {
            khoa = tableKhoa.getSelectionModel().getSelectedItem();
            txtMaKhoa.setText(khoa.getMaKhoa());
            txtTenKhoa.setText(khoa.getTenKhoa());
            txtMaKhoa.setDisable(true);
        }
    }

    @FXML
    private void btnClearClicked(MouseEvent event) {
        clearData();
    }

    public void loadTable() {
        String[] titles = {"ID", "Mã khoa", "Tên khoa"};
        String[] elements = {"id", "maKhoa", "tenKhoa"};
        List<Khoa> listKhoa = khoaDAO.getAll();
        TableHelper<Khoa> tableHelper = new TableHelper<>(tableKhoa, titles, elements, listKhoa);
        tableHelper.setModel();
    }

    public void reloadTable() {
        tableKhoa.getItems().removeAll();
        List<Khoa> list = khoaDAO.getAll();
        ObservableList<Khoa> listData = FXCollections.observableList(list);
        tableKhoa.setItems(listData);
    }
    
    public void clearData() {
        khoa = null;
        txtMaKhoa.clear();
        txtTenKhoa.clear();
        txtMaKhoa.setDisable(false);
        txtTenKhoa.setDisable(false);
        lblErrorMa.setText(null);
        lblErrorTen.setText(null);
        txtMaKhoa.requestFocus();
    }

    

}
