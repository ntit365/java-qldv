/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import static Common.DialogHelper.DIALOG_HELPER;
import DAO.LoginDAO;
import DAO.RoleDAO;
import Model.Role;
import Model.User;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author linhnd
 */
public class SettingController implements Initializable {

    @FXML
    private AnchorPane MainViewPane;
    @FXML
    private Label txtNameLogin;
    @FXML
    private Label txtRole;
    public final int KHOA = 1;
    public final int NGANHHOC = 2;
    public final int KHOAHOC = 3;
    public final int LOP = 4;
    @FXML
    private Button btnQLKhoa;
    @FXML
    private Button btnQLKhoaHoc;
    @FXML
    private Button btnQLNganhHoc;
    @FXML
    private Button btnQLLop;
    @FXML
    private ImageView btnBack1;
    @FXML
    private ImageView btnLogout1;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        loadUserLogin();
        try {
            if (LoginDAO.hasPermissionByName("view_khoa")) {
                loadPane(KHOA);
                btnQLKhoa.getStyleClass().add("item-menu-active");
            } else if (LoginDAO.hasPermissionByName("view_khoahoc")) {
                loadPane(KHOAHOC);
                btnQLKhoaHoc.getStyleClass().add("item-menu-active");
            } else if (LoginDAO.hasPermissionByName("view_lop")) {
                loadPane(LOP);
                btnQLLop.getStyleClass().add("item-menu-active");
            } else if (LoginDAO.hasPermissionByName("view_nganh")) {
                loadPane(NGANHHOC);
                btnQLNganhHoc.getStyleClass().add("item-menu-active");
            } else {
                DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
            }
            
        } catch (IOException ex) {
            Logger.getLogger(SettingController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
     @FXML
    private void btnQLKhoaClicked(MouseEvent event) throws IOException {
         if (LoginDAO.hasPermissionByName("view_khoa")) {
             loadPane(KHOA);
            btnQLKhoa.getStyleClass().add("item-menu-active");
            btnQLKhoaHoc.getStyleClass().remove("item-menu-active");
            btnQLLop.getStyleClass().remove("item-menu-active");
            btnQLNganhHoc.getStyleClass().remove("item-menu-active");
         } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
         }
    }

    @FXML
    private void btnQLKhoaHocClicked(MouseEvent event) throws IOException {
        if (LoginDAO.hasPermissionByName("view_khoahoc")) {
            loadPane(KHOAHOC);
            btnQLKhoa.getStyleClass().remove("item-menu-active");
            btnQLKhoaHoc.getStyleClass().add("item-menu-active");
            btnQLLop.getStyleClass().remove("item-menu-active");
            btnQLNganhHoc.getStyleClass().remove("item-menu-active");
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
    }

    @FXML
    private void btnQLNganhHocClicked(MouseEvent event) throws IOException {
        if (LoginDAO.hasPermissionByName("view_nganh")) {
            loadPane(NGANHHOC);
            btnQLKhoa.getStyleClass().remove("item-menu-active");
            btnQLKhoaHoc.getStyleClass().remove("item-menu-active");
            btnQLLop.getStyleClass().remove("item-menu-active");
            btnQLNganhHoc.getStyleClass().add("item-menu-active");
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
    }

    @FXML
    private void btnQLLopClicked(MouseEvent event) throws IOException {
        if (LoginDAO.hasPermissionByName("view_lop")) {
            loadPane(LOP);
            btnQLKhoa.getStyleClass().remove("item-menu-active");
            btnQLKhoaHoc.getStyleClass().remove("item-menu-active");
            btnQLLop.getStyleClass().add("item-menu-active");
            btnQLNganhHoc.getStyleClass().remove("item-menu-active");
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
    }
    
    private void loadPane(int type) throws IOException {
        switch(type) {
            case 1:{
                AnchorPane loadPane = FXMLLoader.load(getClass().getResource("/View/Setting/QLKhoaUI.fxml"));
                MainViewPane.getChildren().setAll(loadPane);
                break;
            }
            case 2:{
                AnchorPane loadPane = FXMLLoader.load(getClass().getResource("/View/Setting/QLNganhHocUI.fxml"));
                MainViewPane.getChildren().setAll(loadPane);
                break;
            }
            case 3:{
                AnchorPane loadPane = FXMLLoader.load(getClass().getResource("/View/Setting/QLKhoaHocUI.fxml"));
                MainViewPane.getChildren().setAll(loadPane);
                break;
            }
            case 4:{
                AnchorPane loadPane = FXMLLoader.load(getClass().getResource("/View/Setting/QLLopUI.fxml"));
                MainViewPane.getChildren().setAll(loadPane);
                break;
            }
        }
    }
    
    private void loadUserLogin() {
        User user = LoginDAO.getUserLogin();
        txtNameLogin.setText(user.getName());
        RoleDAO roleDAO = new RoleDAO();
        Optional<Role> role = roleDAO.get(user.getIdRole());
        txtRole.setText(role.get().getLabel());
    }

    @FXML
    private void btnLogoutClicked(MouseEvent event) throws IOException {
        LoginDAO.logout();
        Parent root = FXMLLoader.load(getClass().getResource("/View/Login.fxml"));
        Scene scene = new Scene(root);
        Controller.window.setScene(scene);
    }

    @FXML
    private void btnBackClicked(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/View/MainMenu.fxml"));
        Scene scene = new Scene(root);
        Controller.window.setScene(scene);
    }
}
