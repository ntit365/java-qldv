
package Controller;

import static Common.DialogHelper.DIALOG_HELPER;
import Common.TableHelper;
import static DAO.DAOImplement.SUCCESS;
import DAO.KhoaDAO;
import DAO.KhoaHocDAO;
import DAO.LoginDAO;
import DAO.LopDAO;
import DAO.NganhHocDAO;
import DAO.StudentDAO;
import Model.*;
import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import javafx.collections.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;


/**
 * FXML Controller class
 *
 * @author linhnd
 */
public class QLDoanVienUIController implements Initializable {
    @FXML
    private TableView<CustomStudent> tableDoanVien;
    @FXML
    private JFXButton btnThem;
    @FXML
    private JFXButton btnSua;
    @FXML
    private JFXButton btnXoa;
    
    private CustomStudent customStudent = null;
    StudentDAO studentDAO=new StudentDAO();
    private LopDAO lopDAO = null;
    private NganhHocDAO nganhHocDAO = null;
    private KhoaDAO khoaDAO = null;
    private KhoaHocDAO khoaHocDAO = null;
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lopDAO = new LopDAO();
        nganhHocDAO = new NganhHocDAO();
        khoaDAO = new KhoaDAO();
        khoaHocDAO = new KhoaHocDAO();
        LoadTable();
    }
         

    @FXML
    private void btnThemClicked(MouseEvent event) {
        if (LoginDAO.hasPermissionByName("add_doanvien")) {
            this.customStudent = null;
            boolean okClicked = this.showDialogDoanVien(customStudent);
            if (okClicked) {
                DIALOG_HELPER.infoDialog("Thông báo", "Thêm đoàn viên thành công!");
                reloadTable();
            }
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
        
    }

    @FXML
    private void btnSuaClicked(MouseEvent event) {
        if (LoginDAO.hasPermissionByName("update_doanvien")) {
            if (customStudent != null) {
                boolean okClicked = this.showDialogDoanVien(customStudent);
                if (okClicked) {
                    customStudent = null;
                    reloadTable();
                    DIALOG_HELPER.infoDialog("Thông báo", "Cập nhật đoàn viên thành công!");
                }
            } else {
                DIALOG_HELPER.infoDialog("Lỗi", "Vui lòng chọn đoàn viên cần cập nhật!");
            }
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
    }

    @FXML
    private void btnXoaClicked(MouseEvent event) {
        if (LoginDAO.hasPermissionByName("delete_doanvien")) {
            if (customStudent != null) {
                Student student = new Student();
                student.setId(customStudent.getId());
                student.setStudentCode(customStudent.getStudentCode());
                Optional<ButtonType> resultDialog = DIALOG_HELPER.dialogConfirm("Xác nhận", "Bạn có muốn xóa đoàn viên #ID: " + student.getId() + " hay không?");
                if (DIALOG_HELPER.confirmAction(resultDialog)) {
                    int status = studentDAO.delete(student);
                    if (status == SUCCESS) 
                    {
                        customStudent = null;
                        DIALOG_HELPER.infoDialog("Thông báo", "Xóa đoàn viên thành công!");
                        reloadTable();
                    } else  {
                        DIALOG_HELPER.infoDialog("Lỗi", "Đã xảy ra lỗi trong quá trình xóa!");
                    }
                }
            }
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
    }
    
    
    @FXML
    private void tableDoanVienClicked(MouseEvent event) {
        if (tableDoanVien.getSelectionModel().getSelectedItems() != null) {
            customStudent = tableDoanVien.getSelectionModel().getSelectedItem();
        }
    }
  
    private void LoadTable()
    {
        String[] titles = {"Mã sinh viên", "Họ tên", "Giới tính", "Ngày sinh", "Ngày vào Đoàn", "Điện thoại", "Địa chỉ", "Chức vụ", "Sổ đoàn", "Sinh hoạt", "Lớp", "Khóa học"};
        String[] elements = {"studentCode", "hoTen", "gioiTinh", "ngaySinh", "ngayVaoDoan", "dienThoai", "diaChi", "chucVu", "tenSoDoan", "tenSinhHoat", "tenLop", "tenKhoaHoc"};
        List<Student> listStudent = studentDAO.getAll();
        List<CustomStudent> listCustomStudent = new LinkedList<>();
        for (Student a: listStudent)
        {
            CustomStudent ncs = new CustomStudent(a);
            ncs.genKhoaNganhLop();
            listCustomStudent.add(ncs);

        }
        TableHelper<CustomStudent> tableHelper = new TableHelper<>(tableDoanVien, titles, elements, listCustomStudent);
        tableHelper.setModel();
    }
    public void reloadTable()
    {
        tableDoanVien.getItems().removeAll();
        List<Student> list = studentDAO.getAll();
        List<CustomStudent> listCustomStudent = new LinkedList<>();
        for (Student a: list)
        {
            CustomStudent ncs = new CustomStudent(a);
            ncs.genKhoaNganhLop();
            listCustomStudent.add(ncs);
            
        }
        ObservableList<CustomStudent> listData = FXCollections.observableList(listCustomStudent);
        tableDoanVien.setItems(listData);
    }
    
    public boolean showDialogDoanVien(CustomStudent customStudent) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/View/DoanVien/Dialog/DoanVienDialog.fxml"));
            Parent parent = loader.load();
            Scene scene = new Scene(parent);
            Stage dialogStage = new Stage();
            if (customStudent == null) {
                dialogStage.setTitle("Thêm đoàn viên");
            } else {
                dialogStage.setTitle("Sửa đoàn viên");
            }
            dialogStage.initOwner(Controller.window);
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.getIcons().add(new Image("Image/logo-doan.png"));
            dialogStage.setResizable(false);
            dialogStage.setScene(scene);
            DoanVienDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setDoanVien(customStudent);
            dialogStage.showAndWait();
            return controller.isOkClicked();
        } catch (IOException e) {
        }
        return false;
    }
    
    public class CustomStudent extends Student
    {
        private Lop lop ;
        private NganhHoc nganhHoc ;
        private Khoa khoa ;
        private String tenLop ;
        private String tenNganh ;
        private String tenKhoa ;
        private KhoaHoc khoaHoc ;
        private String tenKhoaHoc;
        private String tenSoDoan;
        private String tenSinhHoat;

        public CustomStudent(Student a)
        {
            super.setChucVu(a.getChucVu());
            super.setDiaChi(a.getDiaChi());
            super.setDienThoai(a.getDienThoai());
            super.setGioiTinh(a.getGioiTinh());
            super.setHoTen(a.getHoTen());
            super.setId(a.getId());
            super.setIdDistrict(a.getIdDistrict());
            super.setIdKhoaHoc(a.getIdKhoaHoc());
            super.setIdLop(a.getIdLop());
            super.setIdProvince(a.getIdProvince());
            super.setIdWard(a.getIdWard());
            super.setNgaySinh(a.getNgaySinh());
            super.setNgayVaoDoan(a.getNgayVaoDoan());
            super.setSinhHoat(a.isSinhHoat());
            super.setSoDoan(a.isSoDoan());
            super.setStudentCode(a.getStudentCode());
        }
        public void genKhoaNganhLop()
        {
            lop = lopDAO.get(super.getIdLop()).get();
            nganhHoc = nganhHocDAO.get(lop.getIdNganh()).get();
            khoa = khoaDAO.get(nganhHoc.getIdKhoa()).get();
            khoaHoc = khoaHocDAO.get(super.getIdKhoaHoc()).get();
            tenKhoa = khoa.getTenKhoa();
            tenKhoaHoc = khoaHoc.getTenKhoaHoc();
            tenNganh = nganhHoc.getTenNganhHoc();
            tenLop = lop.getTenLop();
            tenSoDoan = ((super.isSoDoan()==true)?"Đã nộp":"Chưa nộp");
            tenSinhHoat = ((super.isSinhHoat()==true)?"Đã chuyển":"Chưa chuyển");
        }
        public String getTenSoDoan() {
            return tenSoDoan;
        }

        public String getTenSinhHoat() {
            return tenSinhHoat;
        }
        public String getTenLop() {
            return tenLop;
        }
        public String getTenNganh() {
            return tenNganh;
        }
        public String getTenKhoa() {
            return tenKhoa;
        }
        public String getTenKhoaHoc() {
            return tenKhoaHoc;
        }
        public Khoa getKhoa(){
            return khoa;
        }
        public KhoaHoc getKhoaHoc(){
            return khoaHoc;
        }
        public NganhHoc getNganhHoc(){
            return nganhHoc;
        }
        public Lop getLop(){
            return lop;
        }
    }

}