/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Common.BcryptPassword;
import Common.Validate;
import static Common.DialogHelper.DIALOG_HELPER;
import static DAO.DAOImplement.SUCCESS;
import Common.TableHelper;
import DAO.DAOImplement;
import DAO.KhoaDAO;
import DAO.RoleDAO;
import DAO.UserDAO;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import Model.*;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 *
 * @author truon
 */
public class QLTaiKhoanUIController implements Initializable {

    @FXML
    private TableView<User> tblTaiKhoan;
    @FXML
    private JFXButton btnThem;
    @FXML
    private JFXButton btnCapNhat;
    @FXML
    private JFXButton btnXoa;
    @FXML
    private TextField txtUsername;
    @FXML
    private TextField txtPassword;
    @FXML
    private TextField txtRepassword;
    @FXML
    private TextField txtName;
    @FXML
    private ComboBox<Khoa> cboDonVi;
    @FXML
    private ComboBox<Role> cboVaiTro;
    
    private boolean status = false;
    private UserDAO userDAO = null;
    private User user = null;
    @FXML
    private Label lblErrorUsername;
    @FXML
    private Label lblErrorName;
    @FXML
    private Label lblPassword;
    @FXML
    private Label lblRepassword;
    @FXML
    private Label lblErrorDonVi;
    @FXML
    private Label lblErrorVaiTro;
    @FXML
    private JFXButton btnClear;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        userDAO = new UserDAO();
        loadTable();
        loadCboDonVi();
        loadCboVaiTro();
        checkPassword();
        clearData();
    }

    @FXML
    private void tbKhoaClicked(MouseEvent event) {
        if (tblTaiKhoan.getSelectionModel().getSelectedItem() != null) {
            user = tblTaiKhoan.getSelectionModel().getSelectedItem();
            txtName.setText(user.getName());
            txtUsername.setText(user.getUsername());
            txtPassword.setText(user.getPassword());
            txtRepassword.setText(user.getPassword());
            KhoaDAO khoaDAO = new KhoaDAO();
            Optional<Khoa> khoa = khoaDAO.get(user.getIdKhoa());
            cboDonVi.getSelectionModel().select(khoa.get());
            RoleDAO roleDAO = new RoleDAO();
            Optional<Role> role = roleDAO.get(user.getIdRole());
            cboVaiTro.getSelectionModel().select(role.get());
            txtUsername.setDisable(true);
            txtPassword.setDisable(true);
            txtRepassword.setDisable(true);
        }
    }

    @FXML
    private void btnThemClicked(MouseEvent event) throws IOException {
        if (user != null) { 
            user = null;
            clearData();
            DIALOG_HELPER.infoDialog("Lỗi", "Người dùng đã tồn tại! Vui lòng kiểm tra lại!");
        } else {
            boolean isUsername = Validate.requiredTextField(txtUsername, lblErrorUsername, "Username không được để trống!");
            boolean isName = Validate.requiredTextField(txtName, lblErrorName, "Họ tên không được để trống!");
            boolean isPassword = Validate.requiredTextField(txtPassword, lblPassword, "Mật khẩu không được để trống!");
            boolean isRepassword = Validate.requiredTextField(txtRepassword, lblRepassword, "Mật khẩu không được để trống!");
            boolean isDonVi = Validate.requiredCombobox(cboDonVi, lblErrorDonVi, "Vui lòng chọn đơn vị!");
            boolean isVaiTro = Validate.requiredCombobox(cboVaiTro, lblErrorVaiTro, "Vui lòng chọn vai trò!");
            if (isPassword && isRepassword) {
                this.validatePassword("Mật khẩu không trùng khớp");
            }
            if (isVaiTro && isUsername && isPassword && isRepassword && isDonVi && this.status) {
            user = new User();
            user.setIdKhoa(cboDonVi.getValue().getId());
            user.setIdRole(cboVaiTro.getValue().getId());
            user.setUsername(txtUsername.getText());
            user.setPassword(BcryptPassword.hashPassword(txtPassword.getText()));
            user.setName(txtName.getText());
            int result = userDAO.save(user);
            if (result  == SUCCESS) {
                user = null;
                reloadTable();
                clearData();
                DIALOG_HELPER.infoDialog("Thông báo", "Thêm thành công tài khoản người dùng!");
            } else if (result == DAOImplement.DUPLICATE_RESULT) {
                DIALOG_HELPER.infoDialog("Lỗi", "Người dùng đã tồn tại! Vui lòng kiểm tra lại!");
            }
            }
        }  
    }

    @FXML
    private void btnCapNhatClicked(MouseEvent event) {
        if (user != null) {
            boolean isName = Validate.requiredTextField(txtName, lblErrorName, "Họ tên không được để trống!");
            boolean isVaiTro = Validate.requiredCombobox(cboVaiTro, lblErrorVaiTro, "Vui lòng chọn vai trò!");
            boolean isDonVi = Validate.requiredCombobox(cboDonVi, lblErrorDonVi, "Vui lòng chọn đơn vị!");
            if (isName && isVaiTro && isDonVi) {
                user.setIdKhoa(cboDonVi.getValue().getId());
                user.setIdRole(cboVaiTro.getValue().getId());
                user.setName(txtName.getText());
                int result = userDAO.update(user);
                if (result == SUCCESS) {
                    DIALOG_HELPER.infoDialog("Thành công", "Cập nhật người dùng #ID: "+user.getId() + "thành công!");
                    user = null;
                    reloadTable();
                    clearData();
                }
            }
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Vui lòng chọn người dùng bạn cần cập nhật");
        }
    }

    @FXML
    private void btnXoaClicked(MouseEvent event) {
        if (user != null) {
            Optional<ButtonType> resultDialog = DIALOG_HELPER.dialogConfirm("Xác nhận", "Bạn có muốn xóa tài khoản #ID: " + user.getId() + " hay không?");
            if (DIALOG_HELPER.confirmAction(resultDialog)) {
                int result = userDAO.delete(user);
                if (result ==  SUCCESS) {
                    reloadTable();
                    DIALOG_HELPER.infoDialog("Thành công", "Xóa người dùng #ID: "+user.getId() + "thành công!");
                    user = null;
                    clearData();
                }
            }
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Vui lòng chọn người dùng bạn cần xóa");
        }
    }
    
     @FXML
    private void btnClearClicked(MouseEvent event) {
        clearData();
    }
    
    private void validatePassword(String errorMessage) {
        String message = null;
        if (!this.status) {
            message = errorMessage;
        }
        lblPassword.setText(message);
        lblRepassword.setText(message);
        lblRepassword.setTextFill(Color.web("#bc3216"));
        lblPassword.setTextFill(Color.web("#bc3216"));
    }
    
     public void loadTable() {
        String[] titles = {"ID", "Username", "Name", "Đơn vị", "Vai trò"};
        String[] elements = {"id", "username", "name", "khoaName", "roleName"};
        List<User> listUsers = userDAO.getAll();
        TableHelper<User> tableHelper = new TableHelper<>(tblTaiKhoan, titles, elements, listUsers);
        tableHelper.setModel();
    }
    
    public void reloadTable(){
        tblTaiKhoan.getItems().removeAll();
        List<User> list = userDAO.getAll();
        ObservableList<User> listData = FXCollections.observableList(list);
        tblTaiKhoan.setItems(listData);
    }
    
    public void clearData(){
        txtName.clear();
        txtPassword.clear();
        txtRepassword.clear();
        txtUsername.clear();
        txtName.setDisable(false);
        txtUsername.setDisable(false);
        txtPassword.setDisable(false);
        txtRepassword.setDisable(false);
        cboDonVi.setDisable(false);
        cboVaiTro.setDisable(false);
        txtUsername.requestFocus();
        cboDonVi.getSelectionModel().select(null);
        cboVaiTro.getSelectionModel().select(null);
        lblErrorDonVi.setText(null);
        lblErrorVaiTro.setText(null);
        lblErrorUsername.setText(null);
        lblErrorName.setText(null);
        lblPassword.setText(null);
        lblRepassword.setText(null);
        txtUsername.requestFocus();
    }
    
    private void checkPassword() {
        txtRepassword.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            String password = txtPassword.getText();
            this.status = newValue.equals(password);
        });
        txtPassword.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            String repassword = txtRepassword.getText();
            this.status = newValue.equals(repassword);
        });
    }
    
    public void loadCboVaiTro() {
        RoleDAO roleDAO = new RoleDAO();
        List<Role> listRoles = roleDAO.getAll();
        ObservableList<Role> listData = FXCollections.observableList(listRoles);
        cboVaiTro.setItems(listData);
        cboVaiTro.setConverter(new StringConverter<Role>() {

            @Override
            public String toString(Role object) {
                return object.getLabel();
            }

            @Override
            public Role fromString(String string) {
                return cboVaiTro.getItems().stream().filter(ap -> 
                    ap.getLabel().equals(string)).findFirst().orElse(null);
            }
        });
        
    }
    
    public void loadCboDonVi() {
        KhoaDAO khoaDAO = new KhoaDAO();
        List<Khoa> listKhoas = khoaDAO.getAll();
        ObservableList<Khoa> listData = FXCollections.observableList(listKhoas);
        cboDonVi.setItems(listData);
        cboDonVi.setConverter(new StringConverter<Khoa>() {

            @Override
            public String toString(Khoa khoa) {
                return khoa.getTenKhoa();
            }

            @Override
            public Khoa fromString(String string) {
                return cboDonVi.getItems().stream().filter(ap -> 
                    ap.getTenKhoa().equals(string)).findFirst().orElse(null);
            }
        });
    }
    
}
