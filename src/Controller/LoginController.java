/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Common.Validate;
import DAO.LoginDAO;
import Model.User;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
/**
 *
 * @author truon
 */
public class LoginController implements Initializable {
    
    @FXML
    private Button btnLogin;
    @FXML
    private PasswordField txtPassword;
    @FXML
    private TextField txtUsername;
    @FXML
    private Label lblUsername;
    @FXML
    private Label lblPassword;
    
    private boolean isUsername;
    private boolean isPassword;

    @FXML
    private void onLoginClick(ActionEvent event) {
        isPassword = Validate.requiredTextField(txtPassword, lblPassword, "Mật khẩu không được để trống!");
        isUsername = Validate.requiredTextField(txtUsername, lblUsername, "Tài khoản không được để trống!");
        if (isPassword && isUsername) {
            User userLogin = new User();
            userLogin.setUsername(txtUsername.getText());
            userLogin.setPassword(txtPassword.getText());
            login(userLogin);
        }
    }
    
    private void login(User user){
        if (LoginDAO.authenticate(user)) {
            try {
                Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("View/MainMenu.fxml"));
                Scene scene = new Scene(root);
                Controller.window.setScene(scene);
            } catch (IOException ex) {
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
            lblPassword.setText("Tài khoản hoặc mật khẩu không đúng vui lòng thử lại!");
            txtPassword.setText(null);
            txtUsername.setText(null);
            txtUsername.requestFocus();
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lblPassword.setText(null);
        lblUsername.setText(null);
    }

    @FXML
    private void txtUsername_KeyPress(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            isPassword = Validate.requiredTextField(txtPassword, lblPassword, "Mật khẩu không được để trống!");
            isUsername = Validate.requiredTextField(txtUsername, lblUsername, "Tài khoản không được để trống!");
        }   
    }

    @FXML
    private void txtPassword_KeyPress(KeyEvent event) {
         if (event.getCode() == KeyCode.ENTER) {
            isPassword = Validate.requiredTextField(txtPassword, lblPassword, "Mật khẩu không được để trống!");
            isUsername = Validate.requiredTextField(txtUsername, lblUsername, "Tài khoản không được để trống!");
            if (isPassword && isUsername) {
                lblPassword.setText(null);
                lblUsername.setText(null);
                User userLogin = new User();
                userLogin.setUsername(txtUsername.getText());
                userLogin.setPassword(txtPassword.getText());
                login(userLogin);
            }
        }   
    }
    
}
