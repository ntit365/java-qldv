/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import static Common.DialogHelper.DIALOG_HELPER;
import Common.TableHelper;
import static DAO.DAOImplement.SUCCESS;
import DAO.KhoaDAO;
import DAO.KhoaHocDAO;
import DAO.LoginDAO;
import DAO.LopDAO;
import DAO.NganhHocDAO;
import DAO.StudentDAO;
import Model.Khoa;
import Model.KhoaHoc;
import Model.Lop;
import Model.NganhHoc;
import Model.Student;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 *
 * @author linhnd
 */
public class DoanFeeUIController implements Initializable {
    
    @FXML
    private TableView<DoanPhi> tableDoanPhi;
    @FXML
    private ComboBox<KhoaHoc> cbKhoaHoc;
    @FXML
    private ComboBox<Khoa> cbKhoa;
    @FXML
    private ComboBox<Lop> cbLop;
    @FXML
    private RadioButton rdDaDong;
    @FXML
    private ToggleGroup gr;
    @FXML
    private RadioButton rdChuaDong;
    @FXML
    private JFXButton btnLuuLai;
    @FXML
    private JFXButton btnClearAll;
    @FXML
    private JFXTextField txtTimKiem;
    @FXML
    private ComboBox<NganhHoc> cbNganh;
    
    LopDAO lopDAO = new LopDAO();
    KhoaHocDAO khoaHocDAO = new KhoaHocDAO();
    StudentDAO studentDAO=new StudentDAO();
    KhoaDAO khoaDAO = new KhoaDAO();
    NganhHocDAO nganhHocDAO = new NganhHocDAO();
    TableHelper tableHelper = null;
    List<Lop> listLop = null;
    List<KhoaHoc> listKhoaHoc=null;
    List<Khoa> listKhoa = null;
    List<NganhHoc> listNganh = null;
    List<Student> listStudent = null;
    List<DoanPhi> listDoanPhi = null;


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        listLop = lopDAO.getAll();
        listKhoaHoc = khoaHocDAO.getAll();
        listKhoa = khoaDAO.getAll();
        listNganh = nganhHocDAO.getAll();
        LoadcbKhoaHoc();
        LoadcbKhoa();
        loadTable();
    }    
    
    private void LoadcbKhoaHoc()
    {
        ObservableList<KhoaHoc> listData = FXCollections.observableList(listKhoaHoc);
        cbKhoaHoc.setItems(listData);
        cbKhoaHoc.setConverter(new StringConverter<KhoaHoc>() {
            @Override
            public String toString(KhoaHoc khoaHoc) {
                return khoaHoc.getTenKhoaHoc();
            }
            @Override
            public KhoaHoc fromString(String string) {
                return cbKhoaHoc.getItems().stream().filter(ap -> 
                    ap.getTenKhoaHoc().equals(string)).findFirst().orElse(null);
            }
        });
    }
    private void LoadcbKhoa()
    {
        ObservableList<Khoa> listData = FXCollections.observableList(listKhoa);
        cbKhoa.setItems(listData);
        cbKhoa.setConverter(new StringConverter<Khoa>() {
            @Override
            public String toString(Khoa khoa) {
                return khoa.getTenKhoa();
            }
            @Override
            public Khoa fromString(String string) {
                return cbKhoa.getItems().stream().filter(ap -> 
                    ap.getTenKhoa().equals(string)).findFirst().orElse(null);
            }
        });
    }
    private void LoadcbNganh(int idKhoa)
    {
        List<NganhHoc> newListNganh = new LinkedList<>();
        newListNganh = nganhHocDAO.getNganhHocWithIdKhoa(idKhoa);
        ObservableList<NganhHoc> listData = FXCollections.observableList(newListNganh);
        cbNganh.setItems(listData);
        cbNganh.setConverter(new StringConverter<NganhHoc>() {
            @Override
            public String toString(NganhHoc nganh) {
                return nganh.getTenNganhHoc();
            }
            @Override
            public NganhHoc fromString(String string) {
                return cbNganh.getItems().stream().filter(ap -> 
                    ap.getTenNganhHoc().equals(string)).findFirst().orElse(null);
            }
        });
    }
    private void LoadcbLop(int idNganh)
    {
        List <Lop> newListLop = new LinkedList<>();
        newListLop = lopDAO.getLopWithIdNganh(idNganh);
        ObservableList<Lop> listData = FXCollections.observableList(newListLop);
        cbLop.setItems(listData);
        cbLop.setConverter(new StringConverter<Lop>() {

            @Override
            public String toString(Lop lop) {
                return lop.getTenLop();
            }

            @Override
            public Lop fromString(String string) {
                return cbLop.getItems().stream().filter(ap -> 
                    ap.getTenLop().equals(string)).findFirst().orElse(null);
            }
        });
    }
    
    private void loadTable()
    {
        String title[]={"Mã sinh viên", "Tên sinh viên", "Khóa học", "Khoa", "Ngành học", "Lớp", "Trạng thái"};
        String element[]={"studentCode", "hoTen", "tenKhoaHoc", "tenKhoa", "tenNganhHoc", "tenLop", "trangThai" };
        listStudent = studentDAO.getAll();
        listDoanPhi = new LinkedList<>();
        for (Student student : listStudent)
        {
            DoanPhi newDP = new DoanPhi(student);
            newDP.setKhoaLopName();
            listDoanPhi.add(newDP);
        }
        tableHelper = new TableHelper(tableDoanPhi,title,element,listDoanPhi);
        tableHelper.setModel();
    }
    private void reloadTable()
    {
        tableDoanPhi.getItems().removeAll();
        listStudent = studentDAO.getAll();
        listDoanPhi = new LinkedList<>();
        for (Student student : listStudent)
        {
            DoanPhi newDP = new DoanPhi(student);
            newDP.setKhoaLopName();
            listDoanPhi.add(newDP);
        }
        if (cbKhoa.getSelectionModel().getSelectedItem()!=null)
            listDoanPhi = listDoanPhi.stream().filter(u -> u.getKhoa().getId() == cbKhoa.getSelectionModel().getSelectedItem().getId()).collect(Collectors.toList());
        if (cbNganh.getSelectionModel().getSelectedItem()!=null)
            listDoanPhi = listDoanPhi.stream().filter(u -> u.getNganhHoc().getId() == cbNganh.getSelectionModel().getSelectedItem().getId()).collect(Collectors.toList());
        if (cbLop.getSelectionModel().getSelectedItem()!=null)
            listDoanPhi = listDoanPhi.stream().filter(u -> u.getLop().getId() == cbLop.getSelectionModel().getSelectedItem().getId()).collect(Collectors.toList());
        if (cbKhoaHoc.getSelectionModel().getSelectedItem()!=null)
            listDoanPhi = listDoanPhi.stream().filter(u -> u.getKhoaHoc().getId() == cbKhoaHoc.getSelectionModel().getSelectedItem().getId()).collect(Collectors.toList());
        ObservableList<DoanPhi> listData = FXCollections.observableList(listDoanPhi);
        tableDoanPhi.setItems(listData);
    }

    @FXML
    private void tableDoanPhiClicked(MouseEvent event) {
        if (tableDoanPhi.getSelectionModel().getSelectedItem()==null) return ;
        DoanPhi dp = tableDoanPhi.getSelectionModel().getSelectedItem();
        dp.setDiaChiImport();
        int idLop = dp.getIdLop();
        int idNganh = dp.getIdNganhHoc();
        int idKhoa = dp.getIdKhoa();
        rdDaDong.selectedProperty().set(dp.isDoanPhi());
        rdChuaDong.selectedProperty().set(!dp.isDoanPhi());
    }
    

    @FXML
    private void cbKhoaOnAction(ActionEvent event) {
        if (cbKhoa.getSelectionModel().getSelectedItem()==null) return ;
        LoadcbNganh(cbKhoa.getSelectionModel().getSelectedItem().getId());
        cbNganh.getSelectionModel().select(null);
        cbLop.getSelectionModel().select(null);
        reloadTable();
    }

    private void cbNganhHocOnAction(ActionEvent event) {
        if (cbNganh.getSelectionModel().getSelectedItem()==null) return ;
        LoadcbLop(cbNganh.getSelectionModel().getSelectedItem().getId());
        cbLop.getSelectionModel().select(null);
        reloadTable();
    }

    @FXML
    private void cbLopOnAction(ActionEvent event) {
        reloadTable();
    }

    @FXML
    private void btnLuuLaiClicked(MouseEvent event) {
        if (LoginDAO.hasPermissionByName("update_doanphi")) {
            if (tableDoanPhi.getSelectionModel().getSelectedItem()==null) return;
            DoanPhi dp = tableDoanPhi.getSelectionModel().getSelectedItem();
            dp.setDoanPhi(rdDaDong.selectedProperty().getValue());
            Student student = new Student();
            student.setChucVu(dp.getChucVu());
            student.setDienThoai(dp.getDienThoai());
            student.setDoanPhi(dp.isDoanPhi());
            student.setGioiTinh(dp.getGioiTinh());
            student.setHoTen(dp.getHoTen());
            student.setId(dp.getId());
            student.setIdDistrict(dp.getIdDistrict());
            student.setIdKhoaHoc(dp.getIdKhoaHoc());
            student.setIdLop(dp.getIdLop());
            student.setIdProvince(dp.getIdProvince());
            student.setIdWard(dp.getIdWard());
            student.setNgaySinh(dp.getNgaySinh());
            student.setNgayVaoDoan(dp.getNgayVaoDoan());
            student.setSinhHoat(dp.isSinhHoat());
            student.setSoDoan(dp.isSoDoan());
            student.setStudentCode(dp.getStudentCode());
            student.setDiaChiImport();
            student.setDoanPhi(dp.isDoanPhi());
            int status = studentDAO.update(student);
            if (status == SUCCESS) 
            {
                reloadTable();
                DIALOG_HELPER.infoDialog("Thông báo", "Lưu lại thành công!");
            }
            else
            {
                DIALOG_HELPER.infoDialog("Lỗi", "Đã xảy ra lỗi trong quá trình lưu lại!");
            }
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
        
    }

    
    @FXML
    private void btnClearAllClicked(MouseEvent event) {
        if (cbKhoa.getSelectionModel().getSelectedItem()!=null) cbKhoa.getSelectionModel().clearSelection();
        if (cbKhoaHoc.getSelectionModel().getSelectedItem()!=null) cbKhoaHoc.getSelectionModel().clearSelection();
        if (cbNganh.getSelectionModel().getSelectedItem()!=null) cbNganh.getSelectionModel().clearSelection();
        if (cbLop.getSelectionModel().getSelectedItem()!=null) cbLop.getSelectionModel().clearSelection();
        reloadTable();
    }

    @FXML
    private void txtTimkiemOnKeyReleased(KeyEvent event) {
        List <DoanPhi> listFiltered = new LinkedList<>();
        listFiltered = listDoanPhi.stream().filter(u -> (u.getStudentCode().contains(txtTimKiem.getText()) || u.getHoTen().toUpperCase().contains(txtTimKiem.getText().toUpperCase()))).collect(Collectors.toList());
        tableDoanPhi.setItems(FXCollections.observableList(listFiltered));
    }

    @FXML
    private void cbNganhOnAction(ActionEvent event) {
        if (cbNganh.getSelectionModel().getSelectedItem()==null) return ;
        LoadcbLop(cbNganh.getSelectionModel().getSelectedItem().getId());
        cbLop.getSelectionModel().select(null);
        reloadTable();
    }

    @FXML
    private void cbKhoaHocOnAction(ActionEvent event) {
        reloadTable();
    }

    public class DoanPhi extends Student
    {
        private int idKhoa;
        private String tenKhoa;
        private String tenKhoaHoc;
        private int idNganhHoc;
        private String tenNganhHoc;
        private String tenLop;
        private String trangThai;
        private Khoa khoa;
        private KhoaHoc khoaHoc;
        private NganhHoc nganhHoc;
        private Lop lop;

        public DoanPhi(Student a)
        {
            super.setChucVu(a.getChucVu());
            super.setDienThoai(a.getDienThoai());
            super.setDoanPhi(a.isDoanPhi());
            super.setGioiTinh(a.getGioiTinh());
            super.setHoTen(a.getHoTen());
            super.setId(a.getId());
            super.setIdDistrict(a.getIdDistrict());
            super.setIdKhoaHoc(a.getIdKhoaHoc());
            super.setIdLop(a.getIdLop());
            super.setIdProvince(a.getIdProvince());
            super.setIdWard(a.getIdWard());
            super.setNgaySinh(a.getNgaySinh());
            super.setNgayVaoDoan(a.getNgayVaoDoan());
            super.setSinhHoat(a.isSinhHoat());
            super.setSoDoan(a.isSoDoan());
            super.setStudentCode(a.getStudentCode());
            super.setDiaChiImport();
            super.setDiaChi(a.getDiaChi());
        }
        public void setKhoaLopName()
        {
            this.lop = lopDAO.get(super.getIdLop()).get();
            this.nganhHoc = nganhHocDAO.get(lop.getIdNganh()).get();
            this.khoa = khoaDAO.get(nganhHoc.getIdKhoa()).get();
            this.khoaHoc = khoaHocDAO.get(super.getIdKhoaHoc()).get();
            this.idNganhHoc = nganhHoc.getId();
             this.idNganhHoc = nganhHoc.getId();
            this.idKhoa = khoa.getId();
            this.tenKhoa = khoa.getTenKhoa();
            this.tenKhoaHoc = khoa.getTenKhoa();
            this.tenNganhHoc = nganhHoc.getTenNganhHoc();
            this.tenLop = lop.getTenLop();
            this.trangThai = ((super.isDoanPhi()==true)?"Đã đóng":"Chưa đóng");
        }
        
          public Khoa getKhoa() {
            return khoa;
        }

        public KhoaHoc getKhoaHoc() {
            return khoaHoc;
        }

        public NganhHoc getNganhHoc() {
            return nganhHoc;
        }

        public Lop getLop() {
            return lop;
        }

        public int getIdKhoa() {
            return idKhoa;
        }

        public String getTenKhoa() {
            return tenKhoa;
        }

        public String getTenKhoaHoc() {
            return tenKhoaHoc;
        }

        public int getIdNganhHoc() {
            return idNganhHoc;
        }

        public String getTenNganhHoc() {
            return tenNganhHoc;
        }

        public String getTenLop() {
            return tenLop;
        }

        public String getTrangThai() {
            return trangThai;
        }
        
    }

}
