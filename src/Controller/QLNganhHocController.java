/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import static Common.DialogHelper.DIALOG_HELPER;
import Common.TableHelper;
import Common.Validate;
import static DAO.DAOImplement.SUCCESS;
import static DAO.DAOImplement.DUPLICATE_RESULT;
import DAO.KhoaDAO;
import DAO.LoginDAO;
import DAO.NganhHocDAO;
import Model.Khoa;
import Model.NganhHoc;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 *
 * @author Hoang Trang
 */
public class QLNganhHocController implements Initializable {

    @FXML
    private Label lblErrorKhoa;
    @FXML
    private Label lblErrorMa;
    @FXML
    private Label lblErrorTen;
    @FXML
    private TableView<NganhHoc> tableNganhHoc;
    @FXML
    private JFXButton btnThem;
    @FXML
    private JFXButton btnCapNhat;
    @FXML
    private JFXButton btnXoa;
    @FXML
    private TextField txtMaNganhHoc;
    @FXML
    private TextField txtTenNganhHoc;
    @FXML
    private ComboBox<Khoa> cbKhoa;
    @FXML
    private JFXButton btnClear;
    
    List <NganhHoc> listNganhHoc = null;
    NganhHocDAO nganhHocDAO = null;
    KhoaDAO khoaDAO = null;
    NganhHoc nganhHoc = null;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        nganhHocDAO = new NganhHocDAO();
        khoaDAO = new KhoaDAO();
        loadcbKhoa();
        loadTable();
        clearData();
    }    

    @FXML
    private void btnThemClicked(MouseEvent event) {
        if (LoginDAO.hasPermissionByName("add_nganh")) {
            if (nganhHoc != null) {
                nganhHoc = null;
                clearData();
                DIALOG_HELPER.infoDialog("Lỗi", "Ngành học đã tồn tại!");
            } else {
                boolean isKhoa = Validate.requiredCombobox(cbKhoa, lblErrorKhoa, "Vui lòng chọn khoa");
                boolean isTen = Validate.requiredTextField(txtTenNganhHoc, lblErrorTen, "Tên ngành học không được để trống");
                boolean isMa = Validate.requiredTextField(txtMaNganhHoc, lblErrorMa, "Mã ngành học không được để trống");
                if (isMa && isTen && isMa) {
                    nganhHoc = new NganhHoc();
                    nganhHoc.setMaNganhHoc(txtMaNganhHoc.getText());
                    nganhHoc.setTenNganhHoc(txtTenNganhHoc.getText());
                    nganhHoc.setIdKhoa(cbKhoa.getSelectionModel().getSelectedItem().getId());
                    int status = nganhHocDAO.save(nganhHoc);
                    switch (status) {
                        case SUCCESS: {
                            reloadTable();
                            clearData();
                            DIALOG_HELPER.infoDialog("Lỗi", "Thêm ngành học thành công!");
                            break;
                        }
                        case DUPLICATE_RESULT : {
                            DIALOG_HELPER.dialogConfirm("Lỗi", "Ngành học đã tồn tại!");
                            break;
                        }
                        default:
                            DIALOG_HELPER.infoDialog("Lỗi", "Đã xảy ra lỗi trong quá trình thêm!");
                    }
                }
            }
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
    }

    @FXML
    private void btnCapNhatClicked(MouseEvent event) {
        if (LoginDAO.hasPermissionByName("update_nganh")) {
            if (nganhHoc != null) {
                boolean isKhoa = Validate.requiredCombobox(cbKhoa, lblErrorKhoa, "Vui lòng chọn khoa");
                boolean isTen = Validate.requiredTextField(txtTenNganhHoc, lblErrorTen, "Tên ngành học không được để trống");
                if (isTen && isKhoa) {
                    nganhHoc.setTenNganhHoc(txtTenNganhHoc.getText());
                    nganhHoc.setIdKhoa(cbKhoa.getSelectionModel().getSelectedItem().getId());
                    int status = nganhHocDAO.update(nganhHoc);
                    if (status == SUCCESS) {
                        reloadTable();
                        clearData();
                        DIALOG_HELPER.infoDialog("Thông báo", "Cập nhật khóa học thành công!");
                    }
                }
            } else {
                DIALOG_HELPER.infoDialog("Lỗi", "Vui lòng chọn ngành học cần cập nhật!");
            }
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
    }

    @FXML
    private void btnXoaClicked(MouseEvent event) {
        if (LoginDAO.hasPermissionByName("delete_nganh")) {
            if (nganhHoc != null) {
                Optional<ButtonType> resultDialog = DIALOG_HELPER.dialogConfirm("Xác nhận", "Bạn có muốn xóa ngành học #ID: " + nganhHoc.getId() + " hay không?");
                if (DIALOG_HELPER.confirmAction(resultDialog)) {
                    int status = nganhHocDAO.delete(nganhHoc);
                    if(status==SUCCESS) 
                    {
                        reloadTable();
                        clearData();
                        DIALOG_HELPER.infoDialog("Thông báo", "Xóa ngành học thành công!");
                    }
                }
            } else {
                DIALOG_HELPER.infoDialog("Lỗi", "Vui lòng chọn ngành học cần xóa!");
            }
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
    }

    @FXML
    private void tableNganhHocClicked(MouseEvent event) {
        if (tableNganhHoc.getSelectionModel().getSelectedItem() != null) {
            nganhHoc = tableNganhHoc.getSelectionModel().getSelectedItem();
            txtTenNganhHoc.setText(nganhHoc.getTenNganhHoc());
            txtMaNganhHoc.setText(nganhHoc.getMaNganhHoc());
            txtMaNganhHoc.setDisable(true);
            cbKhoa.getSelectionModel().select(khoaDAO.get(nganhHoc.getIdKhoa()).get());
        }
    }
    
    @FXML
    private void btnClearClicked(MouseEvent event) {
        clearData();
    }
    
    private void loadcbKhoa()
    {
        List<Khoa> listKhoa = khoaDAO.getAll();
        cbKhoa.setItems(FXCollections.observableList(listKhoa));
        cbKhoa.setConverter(new StringConverter<Khoa>(){
            @Override
            public String toString(Khoa khoa)
            {
                return khoa.getTenKhoa();
            }

            @Override
            public Khoa fromString(String string) {
                return cbKhoa.getItems().stream().filter(u -> u.getTenKhoa().equals(string)).findFirst().orElse(null);
            }
        });
    }
    
    private void loadTable() {
        String[] titles = {"Mã ngành học", "Tên ngành học","Khoa"};
        String[] elements = {"maNganhHoc", "tenNganhHoc", "tenKhoa"};
        listNganhHoc = nganhHocDAO.getAll();
        TableHelper<NganhHoc> tableHelper = new TableHelper<>(tableNganhHoc, titles, elements, listNganhHoc);
        tableHelper.setModel();
    }
    
    private void reloadTable(){
        tableNganhHoc.getItems().removeAll();
        listNganhHoc = nganhHocDAO.getAll();
        ObservableList<NganhHoc> listData = FXCollections.observableList(listNganhHoc);
        tableNganhHoc.setItems(listData);
    }
    
    private void clearData() {
        nganhHoc = null;
        txtMaNganhHoc.clear();
        txtTenNganhHoc.clear();
        txtMaNganhHoc.setDisable(false);
        txtTenNganhHoc.setDisable(false);
        lblErrorKhoa.setText(null);
        lblErrorMa.setText(null);
        lblErrorTen.setText(null);
        cbKhoa.getSelectionModel().select(-1);
    }
}
