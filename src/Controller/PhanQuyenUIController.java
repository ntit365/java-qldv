/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import static Common.DialogHelper.DIALOG_HELPER;
import Common.TableHelper;
import Common.Validate;
import DAO.DAOImplement;
import static DAO.DAOImplement.SUCCESS;
import DAO.PermissionDAO;
import DAO.RoleDAO;
import Model.Permission;
import Model.Role;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author truon
 */
public class PhanQuyenUIController implements Initializable {

    @FXML
    private TextField txtName;
    @FXML
    private TextField txtLable;
    @FXML
    private TableView<Role> tblRole;
    @FXML
    private Button btnThem;
    @FXML
    private VBox panePermission;
    
    private RoleDAO roleDAO = null;
    private Role role = null;
    private List<CheckBox> listCheckBoxs = null;
    private PermissionDAO permissionDAO = null;
    @FXML
    private Button btnSua;
    @FXML
    private Label lblErrorName;
    @FXML
    private Label lblErrorLable;
    @FXML
    private JFXButton btnClear;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if (roleDAO == null) {
            roleDAO = new RoleDAO();
        }
        if (permissionDAO == null) {
            permissionDAO = new PermissionDAO();
        }
        loadTable();
        loadPermission();
        clearData();
    }    
    
    private void loadTable() {
        String[] titles = {"ID", "Name", "Label"};
        String[] elements = {"id", "name", "label", "khoaName", "roleName"};
        List<Role> listRoles = roleDAO.getAll();
        TableHelper<Role> tableHelper = new TableHelper<>(tblRole, titles, elements, listRoles);
        tableHelper.setModel();
    }

    @FXML
    private void tblRoleClicked(MouseEvent event) {
        if (tblRole.getSelectionModel().getSelectedItem() != null) {
            role = tblRole.getSelectionModel().getSelectedItem();
            txtName.setText(role.getName());
            txtLable.setText(role.getLabel());
            if (role.getName().equals("admin")) {
                txtLable.setDisable(true);
                txtName.setDisable(true);
            } else {
                txtLable.setDisable(false);
                txtName.setDisable(false);
            }
            if (!role.getPermissons().equals("()")) {
                String[] values = role.getPermissons().replaceAll("^\\(|\\)$", "").split(",");
                List<String> listPermissions = new LinkedList<>();
                for (String value : values) {
                    Optional<Permission> permission = permissionDAO.get(Integer.parseInt(value));
                    listPermissions.add(permission.get().getLabel());
                }
                for (int i=0; i<listCheckBoxs.size(); i++) {
                    if (listPermissions.contains(listCheckBoxs.get(i).getText())) {
                        listCheckBoxs.get(i).setSelected(true);
                    } else {
                        listCheckBoxs.get(i).setSelected(false);
                    }
                }
            } else {
                listCheckBoxs.stream().filter((checkBox) -> (checkBox.isSelected())).forEachOrdered((checkBox) -> {
                    checkBox.setSelected(false);
                });
            }
        }
    }
    @FXML
    private void btnThemClicked(ActionEvent event) {
        boolean isName = Validate.requiredTextField(txtName, lblErrorName, "Tên vai trò không được để trống!");
        boolean isLable = Validate.requiredTextField(txtLable, lblErrorLable, "Nhãn vai trò không được để trống!");
        if (isLable && isName) {
           List<Integer> listPermissions = new LinkedList<>();
            for (int i = 0; i < listCheckBoxs.size(); i++) {
                if (listCheckBoxs.get(i).isSelected()) {
                    Optional<Permission> permission = permissionDAO.getPermissionWithLabel(listCheckBoxs.get(i).getText());
                    listPermissions.add(permission.get().getId());
                }
            }
            String permissions = listPermissions.stream()
                .map(n -> String.valueOf(n))
                .collect(Collectors.joining(",", "(", ")"));
            role = new Role();
            role.setName(txtName.getText());
            role.setLabel(txtLable.getText());
            role.setPermissons(permissions);
            int result = roleDAO.save(role);
            switch (result) {
                case SUCCESS:
                    role = null;
                    reloadTable();
                    clearData();
                    DIALOG_HELPER.infoDialog("Thông báo", "Thêm vai trò thành công!");
                    break;
                case DAOImplement.DUPLICATE_RESULT:
                    DIALOG_HELPER.infoDialog("Lỗi", "Vai trò đã tồn tại! Vui lòng kiểm tra lại!");
                    break;
                default:
                    DIALOG_HELPER.infoDialog("Lỗi", "Có lỗi trong quá trình thêm vai trò! Vui lòng kiểm tra lại!");
                    break;
            } 
        }
    }
    @FXML
    private void btnSuaClicked(ActionEvent event) {
        if (role != null) {
            boolean isName = Validate.requiredTextField(txtName, lblErrorName, "Tên vai trò không được để trống!");
            boolean isLable = Validate.requiredTextField(txtLable, lblErrorLable, "Nhãn vai trò không được để trống!");
            if (isLable && isName) {
                List<Integer> listPermissions = new LinkedList<>();
                for (int i = 0; i < listCheckBoxs.size(); i++) {
                    if (listCheckBoxs.get(i).isSelected()) {
                        Optional<Permission> permission = permissionDAO.getPermissionWithLabel(listCheckBoxs.get(i).getText());
                        listPermissions.add(permission.get().getId());
                    }
                }
                String permissions = listPermissions.stream()
                    .map(n -> String.valueOf(n))
                    .collect(Collectors.joining(",", "(", ")"));
                role.setName(txtName.getText());
                role.setLabel(txtLable.getText());
                role.setPermissons(permissions);
                int result = roleDAO.update(role);
                if (result == SUCCESS) {
                    role = null;
                    clearData();
                    reloadTable();
                    DIALOG_HELPER.infoDialog("Thông báo", "Sửa vai trò thành công!");
                } else {
                    DIALOG_HELPER.infoDialog("Có lỗi", "Có lỗi trong quá trình sửa vai trò! Vui lòng kiểm tra lại!");
                }
            }
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn vui lòng chọn vai trò cần sửa");
        }
    }
    
    @FXML
    private void btnClearClicked(ActionEvent event) {
        clearData();
    }
//        if (role != null) {
//            if (!role.getName().equals("admin")) {
//                Optional<ButtonType> resultDialog = DIALOG_HELPER.dialogConfirm("Xác nhận", "Bạn có muốn xóa vai trò " + role.getLabel() + " hay không?");
//                if (DIALOG_HELPER.confirmAction(resultDialog)) {
//                    int result = roleDAO.delete(role);
//                    if (result == SUCCESS) {
//                        role = null;
//                        clearData();
//                        reloadTable();
//                        DIALOG_HELPER.infoDialog("Thông báo", "Xóa vai trò thành công!");
//                    }
//                }
//            } else {
//                DIALOG_HELPER.infoDialog("Thông báo", "Bạn không thể xóa vai trò admin!");
//            }
//        } else {
//            DIALOG_HELPER.infoDialog("Lỗi", "Bạn vui lòng chọn vai trò cần xóa");
//        }
//    }
    
    private void clearData() {
        txtLable.clear();
        txtName.clear();
        lblErrorLable.setText(null);
        lblErrorName.setText(null);
        listCheckBoxs.stream().filter((checkBox) -> (checkBox.isSelected())).forEachOrdered((checkBox) -> {
            checkBox.setSelected(false);
        });
        txtName.requestFocus();
    }
    
    public void reloadTable(){
        tblRole.getItems().removeAll();
        List<Role> list = roleDAO.getAll();
        ObservableList<Role> listData = FXCollections.observableList(list);
        tblRole.setItems(listData);
    }
    
    private void loadPermission() {
        List<Permission> listPermissions = permissionDAO.getAll();
        listCheckBoxs = new LinkedList<>();
        panePermission.setSpacing(10);
        panePermission.setPadding(new Insets(20,20,20,20));
        listPermissions.stream().map((permission) -> {
            return permission;
        }).map((permission) -> new CheckBox(permission.getLabel())).forEachOrdered((checkBox) -> {
            panePermission.getChildren().add(checkBox);
            listCheckBoxs.add(checkBox);
        });
    }
}
