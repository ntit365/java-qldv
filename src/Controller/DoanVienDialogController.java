/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import static Common.DialogHelper.DIALOG_HELPER;
import Common.GenerateStudentCode;
import Common.Validate;
import Controller.QLDoanVienUIController.CustomStudent;
import static DAO.DAOImplement.DUPLICATE_RESULT;
import static DAO.DAOImplement.SUCCESS;
import DAO.DistrictDAO;
import DAO.KhoaDAO;
import DAO.KhoaHocDAO;
import DAO.LopDAO;
import DAO.NganhHocDAO;
import DAO.ProvinceDAO;
import DAO.StudentDAO;
import DAO.WardDAO;
import Model.DataComboBox;
import Model.District;
import Model.Khoa;
import Model.KhoaHoc;
import Model.Lop;
import Model.NganhHoc;
import Model.Province;
import Model.Student;
import Model.Ward;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.StringConverter;
/**
 * FXML Controller class
 *
 * @author truon
 */
public class DoanVienDialogController implements Initializable {
    
    private List<Student> listStudents;
    @FXML
    private ComboBox<NganhHoc> cbNganh;
    @FXML
    private ComboBox<Lop> cbLop;
    @FXML
    private TextField txtMaSinhVien;
    @FXML
    private ComboBox<Khoa> cbKhoa;
    @FXML
    private ComboBox<KhoaHoc> cbKhoaHoc;
    @FXML
    private ComboBox<DataComboBox> cbSinhHoat;
    @FXML
    private ComboBox<DataComboBox> cbSoDoan;
    @FXML
    private ComboBox<Ward> cbXaPhuong;
    @FXML
    private ComboBox<District> cbQuanHuyen;
    @FXML
    private ComboBox<Province> cbTinhTP;
    @FXML
    private ComboBox<DataComboBox> cbChucVu;
    @FXML
    private DatePicker dpkNgayVaoDoan;
    @FXML
    private DatePicker dpkNgaySinh;
    @FXML
    private ComboBox<DataComboBox> cbGioiTinh;
    @FXML
    private TextField txtTenSinhVien;
    @FXML
    private TextField txtSoDienThoai;
    @FXML
    private JFXButton btnOK;
    @FXML
    private JFXButton btnCancel;
    @FXML
    private Label lblErrorTen;
    @FXML
    private Label lblErrorNganh;
    @FXML
    private Label lblErrorNS;
    @FXML
    private Label lblErrorKH;
    @FXML
    private Label lblErrorSD;
    @FXML
    private Label lblErrorHuyen;
    @FXML
    private Label lblErrorND;
    @FXML
    private Label lblErrorMa;
    @FXML
    private Label lblErrorTinh;
    @FXML
    private Label lblErrorXa;
    @FXML
    private Label lblErrorSH;
    @FXML
    private Label lblErrorKhoa;
    @FXML
    private Label lblErrorGT;
    @FXML
    private Label lblErrorLop;
    
    private Stage dialogStage;
    private CustomStudent customStudent = null;
    private boolean okClicked = false;
    private ProvinceDAO provinceDAO = null;
    private DistrictDAO districtDAO = null;
    private WardDAO wardDAO = null; 
    private KhoaHocDAO khoaHocDAO = null;
    private LopDAO lopDAO = null;
    private KhoaDAO khoaDAO = null;
    private NganhHocDAO nganhHocDAO = null;
    private StudentDAO studentDAO = null;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        provinceDAO = new ProvinceDAO();
        districtDAO = new DistrictDAO();
        wardDAO = new WardDAO();
        khoaHocDAO = new KhoaHocDAO();
        khoaDAO = new KhoaDAO();
        lopDAO = new LopDAO();
        nganhHocDAO = new NganhHocDAO();
        studentDAO = new StudentDAO();
        InitComboBoxes();
        loadCboGioiTinh();
        loadCboChucVu();
        loadCboSinhHoat();
        loadCboSoDoan();
        loadCboTinhTP();
        loadCboKhoaHoc();
        loadCboKhoa();
        clearData();
    }    

    @FXML
    private void cbNganhOnAction(ActionEvent event) {
        if (cbNganh.getSelectionModel().getSelectedItem() != null){
            loadCboLop(cbNganh.getSelectionModel().getSelectedItem().getId());
            cbLop.getSelectionModel().select(null);
        }
    }

    @FXML
    private void cbKhoaOnAction(ActionEvent event) {
        if (cbKhoa.getSelectionModel().getSelectedItem() != null) {
            loadCboNganh(cbKhoa.getSelectionModel().getSelectedItem().getId());
            cbNganh.getSelectionModel().select(null);
            cbLop.getSelectionModel().select(null);
        }
    }

    @FXML
    private void cbQuanHuyenOnAction(ActionEvent event) {
        if (cbQuanHuyen.getSelectionModel().getSelectedItem() != null){
            loadCboXaPhuong(cbQuanHuyen.getSelectionModel().getSelectedItem().getId());
            cbXaPhuong.getSelectionModel().select(null);
        }  
    }

    @FXML
    private void cbTinhTPOnAction(ActionEvent event) {
        if (cbTinhTP.getSelectionModel().getSelectedItem() != null){
            loadCboQuanHuyen(cbTinhTP.getSelectionModel().getSelectedItem().getId());
            cbQuanHuyen.getSelectionModel().select(null);
            cbXaPhuong.getSelectionModel().select(null);
        }
    }
    
    @FXML
    private void cbKhoaHocOnAction(ActionEvent event) {
        if (cbKhoaHoc.getSelectionModel().getSelectedItem() != null) {
            Student student = new Student();
            student.setIdKhoaHoc(cbKhoaHoc.getSelectionModel().getSelectedItem().getId());
            txtMaSinhVien.setText(GenerateStudentCode.Generate(student));
        } else {
            txtMaSinhVien.setText(null);
        }
    }
    
     @FXML
    private void btnOkClicked(ActionEvent event) {
         if (validateForm()) {
             if (customStudent == null) {
                Student student = new Student();
                student.setStudentCode(txtMaSinhVien.getText());
                student.setHoTen(txtTenSinhVien.getText());
                student.setGioiTinh(cbGioiTinh.getSelectionModel().getSelectedItem().getValue());
                student.setNgaySinh(dpkNgaySinh.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                student.setNgayVaoDoan(dpkNgayVaoDoan.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                student.setDienThoai(txtSoDienThoai.getText());
                student.setIdProvince(cbTinhTP.getSelectionModel().getSelectedItem().getId());
                student.setIdDistrict(cbQuanHuyen.getSelectionModel().getSelectedItem().getId());
                student.setIdWard(cbXaPhuong.getSelectionModel().getSelectedItem().getId());
                student.setChucVu(cbChucVu.getSelectionModel().getSelectedItem().getValue());
                student.setSinhHoat(cbSinhHoat.getSelectionModel().getSelectedItem().getId().equals("1"));
                student.setSoDoan(cbSoDoan.getSelectionModel().getSelectedItem().getId().equals("1"));
                student.setIdLop(cbLop.getSelectionModel().getSelectedItem().getId());
                student.setIdKhoaHoc(cbKhoaHoc.getSelectionModel().getSelectedItem().getId());
                KhoaHoc kh = khoaHocDAO.get(cbKhoaHoc.getSelectionModel().getSelectedItem().getId()).get();
                kh.setLastCode(kh.getLastCode()+1);
                int status = studentDAO.save(student);
                switch (status) {
                    case SUCCESS: {
                        khoaHocDAO.updateLastCode(kh);
                        dialogStage.close();
                        okClicked = true;
                        break;
                    }
                    case DUPLICATE_RESULT: {
                        DIALOG_HELPER.infoDialog("Lỗi", "Đoàn viên vừa thêm đã tồn tại!");
                        break;
                    }
                    default:
                        DIALOG_HELPER.infoDialog("Lỗi", "Có lỗi trong quá trình thêm đoàn viên! Vui lòng thử lại!");
                        break;
                }
            } else {
                 if (validateForm()) {
                    Student student = new Student();
                    student.setId(customStudent.getId());
                    student.setStudentCode(txtMaSinhVien.getText());
                    student.setHoTen(txtTenSinhVien.getText());
                    student.setNgaySinh(dpkNgaySinh.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                    student.setNgayVaoDoan(dpkNgayVaoDoan.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                    student.setDienThoai(txtSoDienThoai.getText());
                    student.setGioiTinh(cbGioiTinh.getSelectionModel().getSelectedItem().getValue());
                    student.setChucVu(cbChucVu.getSelectionModel().getSelectedItem().getValue());
                    student.setIdProvince(cbTinhTP.getSelectionModel().getSelectedItem().getId());
                    student.setIdDistrict(cbQuanHuyen.getSelectionModel().getSelectedItem().getId());
                    student.setIdWard(cbXaPhuong.getSelectionModel().getSelectedItem().getId());
                    student.setIdLop(cbLop.getSelectionModel().getSelectedItem().getId());
                    student.setSoDoan(cbSoDoan.getSelectionModel().getSelectedItem().getId().equals("1"));
                    student.setIdKhoaHoc(cbKhoaHoc.getSelectionModel().getSelectedItem().getId());
                    int status = studentDAO.update(student);
                     System.out.println(status);
                     if (status == SUCCESS) {
                        dialogStage.close();
                        okClicked = true;
                     }
                }
            }
        }
    }
    
     @FXML
    private void btnCancelClicked(ActionEvent event) {
        dialogStage.close();
    }
    
    public void setDoanVien(CustomStudent customStudent) {
        this.customStudent = customStudent;
        if (customStudent == null) {
            btnOK.setText("Thêm");
            btnOK.getStyleClass().add("btn-add");
            btnOK.getStyleClass().remove("btn-update");
        } else {
            btnOK.setText("Cập nhật");
            btnOK.getStyleClass().add("btn-update");
            btnOK.getStyleClass().remove("btn-add");
            txtTenSinhVien.setText(customStudent.getHoTen());
            txtMaSinhVien.setText(customStudent.getStudentCode());
            txtSoDienThoai.setText(customStudent.getDienThoai());
            if(customStudent.getGioiTinh() == null) 
                cbGioiTinh = null;
            else 
                cbGioiTinh.getSelectionModel().select(new DataComboBox("0",customStudent.getGioiTinh()));
            if (customStudent.getChucVu()==null) 
                cbChucVu = null;
            else 
                cbChucVu.getSelectionModel().select(new DataComboBox("0",customStudent.getChucVu()));
            Lop lop = lopDAO.get(customStudent.getIdLop()).get();
            NganhHoc nganhHoc = nganhHocDAO.get(lop.getIdNganh()).get();
            Khoa khoa = khoaDAO.get(nganhHoc.getIdKhoa()).get();
            cbKhoa.getSelectionModel().select(khoa);
            loadCboNganh(nganhHoc.getIdKhoa());
            cbNganh.getSelectionModel().select(nganhHoc);
            loadCboLop(nganhHoc.getId());
            cbLop.getSelectionModel().select(lop);
            KhoaHoc khoaHoc = khoaHocDAO.get(customStudent.getIdKhoaHoc()).get();
            cbKhoaHoc.getSelectionModel().select(khoaHoc);
            Province province = provinceDAO.getProvinceWithId(customStudent.getIdProvince());
            cbTinhTP.getSelectionModel().select(province);
            loadCboQuanHuyen(customStudent.getIdProvince());
            District district = districtDAO.getDistrictWithId(customStudent.getIdDistrict());
            cbQuanHuyen.getSelectionModel().select(district);
            loadCboXaPhuong(customStudent.getIdDistrict());
            Ward ward = wardDAO.getWardWithId(customStudent.getIdWard());
            cbXaPhuong.getSelectionModel().select(ward);
            if (customStudent.isSinhHoat()) 
                cbSinhHoat.getSelectionModel().select(new DataComboBox("1","Có"));
            else 
                cbSinhHoat.getSelectionModel().select(new DataComboBox("0","Không"));
            if (customStudent.isSoDoan()) cbSoDoan.getSelectionModel().select(new DataComboBox("1","Có"));
            else cbSoDoan.getSelectionModel().select(new DataComboBox("0","Không"));
            dpkNgaySinh.setValue(StringToLocalDate(customStudent.getNgaySinh()));
            dpkNgayVaoDoan.setValue(StringToLocalDate(customStudent.getNgayVaoDoan()));
            txtMaSinhVien.setDisable(true);
            cbSinhHoat.setDisable(true);
        }
    }
    
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
    
    public boolean isOkClicked() {
        return okClicked;
    }
    
    private void InitComboBoxes()
    {
        cbGioiTinh.setItems(FXCollections.observableArrayList(  new DataComboBox("0","Nam"),
                                                                new DataComboBox("1","Nữ")
        ));
        cbChucVu.setItems(FXCollections.observableArrayList(new DataComboBox("0","Lớp trưởng"),
                                                            new DataComboBox("1","Bí thư")
        ));
        cbSoDoan.setItems(FXCollections.observableArrayList(new DataComboBox("1","Có"),new DataComboBox("0","Không")));
        cbSinhHoat.setItems(FXCollections.observableArrayList(new DataComboBox("1","Có"),new DataComboBox("0","Không")));

    }
    
    private void loadCboGioiTinh()
    {
        cbGioiTinh.setConverter(new StringConverter<DataComboBox>() {
            @Override
            public String toString(DataComboBox object) {
                return object.getValue();
            }
            @Override
            public DataComboBox fromString(String string) {
                return cbGioiTinh.getItems().stream().filter(ap -> 
                    ap.getValue().equals(string)).findFirst().orElse(null);
            }
        });
    }
    
    private void loadCboChucVu()
    {
        cbChucVu.setConverter(new StringConverter<DataComboBox>() {
            @Override
            public String toString(DataComboBox object) {
                return object.getValue();
            }
            @Override
            public DataComboBox fromString(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }
    
    private void loadCboSinhHoat()
    {
        cbSinhHoat.setConverter(new StringConverter<DataComboBox>() {
            @Override
            public String toString(DataComboBox object) {
                return object.getValue();
            }
            @Override
            public DataComboBox fromString(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }
    
    private void loadCboSoDoan()
    {
        cbSoDoan.setConverter(new StringConverter<DataComboBox>() {
            @Override
            public String toString(DataComboBox object) {
                return object.getValue();
            }
            @Override
            public DataComboBox fromString(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }
    
    private void loadCboTinhTP()
    {
        List<Province> listProvinces = provinceDAO.getAll();
        ObservableList<Province> listData = FXCollections.observableList(listProvinces);
        cbTinhTP.setItems(listData);
        cbTinhTP.setConverter(new StringConverter<Province>() {
            @Override
            public String toString(Province province) {
                return province.getName();
            }
            @Override
            public Province fromString(String string) {
                return cbTinhTP.getItems().stream().filter(ap -> 
                    ap.getName().equals(string)).findFirst().orElse(null);
            }
        });
    }
    
    private void loadCboQuanHuyen(String idProvince)
    {
        List<District> listDistricts = districtDAO.getDistrictWithIdProvince(idProvince);
        ObservableList<District> listData = FXCollections.observableList(listDistricts);
        cbQuanHuyen.setItems(listData);
        cbQuanHuyen.setConverter(new StringConverter<District>() {
            @Override
            public String toString(District district) {
                return district.getName();
            }
            @Override
            public District fromString(String string) {
                return cbQuanHuyen.getItems().stream().filter(ap -> 
                    ap.getName().equals(string)).findFirst().orElse(null);
            }
        });
    }
    
    private void loadCboXaPhuong(String idDistrict)
    {
        List<Ward> listWards = wardDAO.getWardWithIdDistrict(idDistrict);
        ObservableList<Ward> listData = FXCollections.observableList(listWards);
        cbXaPhuong.setItems(listData);
        cbXaPhuong.setConverter(new StringConverter<Ward>() {
            @Override
            public String toString(Ward ward) {
                return ward.getName();
            }
            @Override
            public Ward fromString(String string) {
                return cbXaPhuong.getItems().stream().filter(ap -> 
                    ap.getName().equals(string)).findFirst().orElse(null);
            }
        });
    }
    
    private void loadCboKhoaHoc()
    {
        List<KhoaHoc> listKhoaHocs = khoaHocDAO.getAll();
        ObservableList<KhoaHoc> listData = FXCollections.observableList(listKhoaHocs);
        cbKhoaHoc.setItems(listData);
        cbKhoaHoc.setConverter(new StringConverter<KhoaHoc>() {
            @Override
            public String toString(KhoaHoc khoaHoc) {
                return khoaHoc.getTenKhoaHoc();
            }
            @Override
            public KhoaHoc fromString(String string) {
                return cbKhoaHoc.getItems().stream().filter(ap -> 
                    ap.getTenKhoaHoc().equals(string)).findFirst().orElse(null);
            }
        });
    }
    
    private void loadCboKhoa()
    {
        List<Khoa> listKhoas = khoaDAO.getAll();
        ObservableList<Khoa> listData = FXCollections.observableList(listKhoas);
        cbKhoa.setItems(listData);
        cbKhoa.setConverter(new StringConverter<Khoa>() {
            @Override
            public String toString(Khoa khoa) {
                return khoa.getTenKhoa();
            }
            @Override
            public Khoa fromString(String string) {
                return cbKhoa.getItems().stream().filter(ap -> 
                    ap.getTenKhoa().equals(string)).findFirst().orElse(null);
            }
        });
    }
    
    private void loadCboNganh(int idKhoa)
    {
        List<NganhHoc> listNganhHocs = nganhHocDAO.getNganhHocWithIdKhoa(idKhoa);
        ObservableList<NganhHoc> listData = FXCollections.observableList(listNganhHocs);
        cbNganh.setItems(listData);
        cbNganh.setConverter(new StringConverter<NganhHoc>() {
            @Override
            public String toString(NganhHoc nganh) {
                return nganh.getTenNganhHoc();
            }
            @Override
            public NganhHoc fromString(String string) {
                return cbNganh.getItems().stream().filter(ap -> 
                    ap.getTenNganhHoc().equals(string)).findFirst().orElse(null);
            }
        });
    }
    
    private void loadCboLop(int idNganh)
    {
        List<Lop> listLops = lopDAO.getLopWithIdNganh(idNganh);
        ObservableList<Lop> listData = FXCollections.observableList(listLops);
        cbLop.setItems(listData);
        cbLop.setConverter(new StringConverter<Lop>() {

            @Override
            public String toString(Lop lop) {
                return lop.getTenLop();
            }

            @Override
            public Lop fromString(String string) {
                return cbLop.getItems().stream().filter(ap -> 
                    ap.getTenLop().equals(string)).findFirst().orElse(null);
            }
        });
    }
    
    public final LocalDate StringToLocalDate (String dateString){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(dateString, formatter);
        return localDate;
    }
    
    private boolean validateForm() {
        boolean isTen = Validate.requiredTextField(txtTenSinhVien, lblErrorTen, "Tên sinh viên không được để trống!");
        boolean isMa = Validate.requiredTextField(txtMaSinhVien, lblErrorMa, "Mã sinh viên không được để trống!");
        boolean isGT = Validate.requiredCombobox(cbGioiTinh, lblErrorGT, "Vui lòng chọn giới tính");
        boolean isQH = Validate.requiredCombobox(cbQuanHuyen, lblErrorHuyen, "Vui lòng chọn quận/huyện");
        boolean isSD = Validate.requiredCombobox(cbSoDoan, lblErrorGT, "Vui lòng chọn sổ đoàn");
        boolean isKH = Validate.requiredCombobox(cbKhoaHoc, lblErrorKH, "Vui lòng chọn khóa học");
        boolean isNganh = Validate.requiredCombobox(cbGioiTinh, lblErrorNganh, "Vui lòng chọn ngành học");
        boolean isT = Validate.requiredCombobox(cbTinhTP, lblErrorTinh, "Vui lòng chọn tỉnh/thành phố");
        boolean isXa = Validate.requiredCombobox(cbXaPhuong, lblErrorXa, "Vui lòng chọn xã/phường");
        boolean isSH = Validate.requiredCombobox(cbSinhHoat, lblErrorSH, "Vui lòng chọn sinh hoạt");
        boolean isKhoa = Validate.requiredCombobox(cbKhoa, lblErrorKhoa, "Vui lòng chọn khoa");
        boolean isLop = Validate.requiredCombobox(cbLop, lblErrorLop, "Vui lòng chọn lớp");
        return isGT && isKH && isKhoa && isLop && isMa && isNganh && isQH && isSD && isKH && isNganh && isT && isXa && isSH && isKhoa && isLop;
    }
    
    private void clearData() {
        lblErrorGT.setText(null);
        lblErrorHuyen.setText(null);
        lblErrorKH.setText(null);
        lblErrorKhoa.setText(null);
        lblErrorLop.setText(null);
        lblErrorMa.setText(null);
        lblErrorND.setText(null);
        lblErrorNS.setText(null);
        lblErrorNganh.setText(null);
        lblErrorSD.setText(null);
        lblErrorSH.setText(null);
        lblErrorTen.setText(null);
        lblErrorTinh.setText(null);
        lblErrorXa.setText(null);
    }
}
