/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.LoginDAO;
import DAO.RoleDAO;
import Model.Role;
import Model.User;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import static Common.DialogHelper.DIALOG_HELPER;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author linhnd
 */
public class UserUIController implements Initializable {

    @FXML
    private AnchorPane MainViewPane;
    @FXML
    private Label txtNameLogin;
    @FXML
    private Label txtRole;
    @FXML
    private Button btnQLTaiKhoan;
    @FXML
    private Button btnTaiKhoan;
    @FXML
    private Button btnPhanQuyen;

    public final int QLTK = 1;
    public final int QLTT = 2;
    public final int QLPQ = 3;
    @FXML
    private ImageView btnBack;
    @FXML
    private ImageView btnLogout;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        loadUserLogin();
        try {
            if (LoginDAO.hasRoleByName("admin")) {
                loadPane(QLTK);
                btnQLTaiKhoan.getStyleClass().add("item-menu-active");
            } else {
                loadPane(QLTT);
                btnTaiKhoan.getStyleClass().add("item-menu-active");
            }
        } catch (IOException ex) {
            Logger.getLogger(UserUIController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void btnQLTaiKhoanClicked(MouseEvent event) throws IOException {
        if (LoginDAO.hasRoleByName("admin")) {
            loadPane(QLTK);
            btnQLTaiKhoan.getStyleClass().add("item-menu-active");
            btnPhanQuyen.getStyleClass().remove("item-menu-active");
            btnTaiKhoan.getStyleClass().remove("item-menu-active");
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập mục này!");
        }
    }

    @FXML
    private void btnTaiKhoanClicked(MouseEvent event) throws IOException {
        loadPane(QLTT);
        btnQLTaiKhoan.getStyleClass().remove("item-menu-active");
        btnPhanQuyen.getStyleClass().remove("item-menu-active");
        btnTaiKhoan.getStyleClass().add("item-menu-active");
    }

    @FXML
    private void btnPhanQuyenClicked(MouseEvent event) throws IOException {
        if (LoginDAO.hasRoleByName("admin")) {
            loadPane(QLPQ);
            btnQLTaiKhoan.getStyleClass().remove("item-menu-active");
            btnPhanQuyen.getStyleClass().add("item-menu-active");
            btnTaiKhoan.getStyleClass().remove("item-menu-active");
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập mục này!");
        }
    }

    private void loadPane(int type) throws IOException {
        switch (type) {
            case 1: {
                AnchorPane loadPane = FXMLLoader.load(getClass().getResource("/View/User/QLTaiKhoanUI.fxml"));
                MainViewPane.getChildren().setAll(loadPane);
                break;
            }
            case 2: {
                AnchorPane loadPane = FXMLLoader.load(getClass().getResource("/View/User/TTTaiKhoanUI.fxml"));
                MainViewPane.getChildren().setAll(loadPane);
                break;
            }
            case 3: {
                AnchorPane loadPane = FXMLLoader.load(getClass().getResource("/View/User/PhanQuyenUI.fxml"));
                MainViewPane.getChildren().setAll(loadPane);
                break;
            }
        }
    }

    private void loadUserLogin() {
        User user = LoginDAO.getUserLogin();
        txtNameLogin.setText(user.getName());
        RoleDAO roleDAO = new RoleDAO();
        Optional<Role> role = roleDAO.get(user.getIdRole());
        txtRole.setText(role.get().getLabel());
    }

    @FXML
    private void btnBackClicked(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/View/MainMenu.fxml"));
        Scene scene = new Scene(root);
        Controller.window.setScene(scene);
    }

    @FXML
    private void btnLogoutClicked(MouseEvent event) throws IOException {
        LoginDAO.logout();
        Parent root = FXMLLoader.load(getClass().getResource("/View/Login.fxml"));
        Scene scene = new Scene(root);
        Controller.window.setScene(scene);
    }

}
