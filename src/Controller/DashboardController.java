/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.LoginDAO;
import DAO.StudentDAO;
import DAO.UserDAO;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author truon
 */
public class DashboardController implements Initializable {

    @FXML
    private PieChart pieChartSH;
    @FXML
    private Label lblDoanVien;
    @FXML
    private Label lblSinhHoat;
    @FXML
    private Label lblSoDoan;
    @FXML
    private Label lblUser;
    @FXML
    private PieChart pieChartSD;
    
    private StudentDAO studentDAO = null;
    
    private UserDAO userDAO = null;
    @FXML
    private ImageView btnBack;
    @FXML
    private ImageView btnLogout;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if (studentDAO == null) {
            studentDAO = new StudentDAO();
        }
        
        if (userDAO == null) {
            userDAO = new UserDAO();
        }
        
        drawChartSinhHoat();
        updateData();
        drawChartSoDoan();
    }
    
    private void updateData(){
        lblDoanVien.setText(String.valueOf(studentDAO.getAll().size()));
        lblSinhHoat.setText(String.valueOf(studentDAO.getListSinhHoat(true).size()));
        lblSoDoan.setText(String.valueOf(studentDAO.getListSoDoan(true).size()));
        lblUser.setText(String.valueOf(userDAO.getAll().size()));
    }
    private void drawChartSinhHoat(){
        PieChart.Data isSH = new PieChart.Data("Sinh hoạt", Double.valueOf(studentDAO.getListSinhHoat(true).size()));
        PieChart.Data notSH = new PieChart.Data("Đã chuyển", Double.valueOf(studentDAO.getListSinhHoat(false).size()));
        pieChartSH.getData().add(isSH);
        pieChartSH.getData().add(notSH);
        pieChartSH.setLegendSide(Side.BOTTOM);
    }
    
    private void drawChartSoDoan(){
        PieChart.Data isSD = new PieChart.Data("Đã nộp", Double.valueOf(studentDAO.getListSoDoan(true).size()));
        PieChart.Data notSD = new PieChart.Data("Chưa nộp", Double.valueOf(studentDAO.getListSoDoan(false).size()));
        pieChartSD.getData().add(isSD);
        pieChartSD.getData().add(notSD);
        pieChartSD.setLegendSide(Side.BOTTOM);
    }    

    @FXML
    private void btnLogoutClicked(MouseEvent event) throws IOException {
        LoginDAO.logout();
        Parent root = FXMLLoader.load(getClass().getResource("/View/Login.fxml"));
        Scene scene = new Scene(root);
        Controller.window.setScene(scene);
    }

    @FXML
    private void btnBacktClicked(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/View/MainMenu.fxml"));
        Scene scene = new Scene(root);
        Controller.window.setScene(scene);
    }
}
