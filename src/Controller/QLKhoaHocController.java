/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import static Common.DialogHelper.DIALOG_HELPER;
import Common.TableHelper;
import Common.Validate;
import static DAO.DAOImplement.SUCCESS;
import static DAO.DAOImplement.DUPLICATE_RESULT;
import DAO.KhoaHocDAO;
import DAO.LoginDAO;
import Model.*;
import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;


/**
 * FXML Controller class
 *
 * @author linhnd
 */
public class QLKhoaHocController implements Initializable {
    @FXML
    private JFXButton btnThem;
    @FXML
    private JFXButton btnCapNhat;
    @FXML
    private JFXButton btnXoa;
    @FXML
    private TextField txtTenKhoaHoc;
    @FXML
    private TextField txtMaKhoaHoc;
    @FXML
    private TableView<KhoaHoc> tableKhoaHoc;
    
    private KhoaHocDAO khoaHocDAO = null;
    private KhoaHoc khoaHoc = null;
    @FXML
    private Label lblErrorMa;
    @FXML
    private Label lblErrorTen;
    @FXML
    private JFXButton btnXoa1;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        khoaHocDAO = new KhoaHocDAO();
        loadTable();
        clearData();
    }

    @FXML
    private void btnThemClicked(MouseEvent event) throws IOException {
        if (LoginDAO.hasPermissionByName("add_khoahoc")) {
            if (khoaHoc != null) {
                khoaHoc = null;
                clearData();
                DIALOG_HELPER.infoDialog("Lỗi", "Khóa học đã tồn tại!");
            } else {
                boolean isMaKhoaHoc = Validate.requiredTextField(txtMaKhoaHoc, lblErrorMa, "Mã khóa học không được để trống!");
                boolean isTenKhoaHoc = Validate.requiredTextField(txtTenKhoaHoc, lblErrorTen, "Tên khóa học không được để trống!");
                if (isTenKhoaHoc && isMaKhoaHoc) {
                    khoaHoc =new KhoaHoc();
                    khoaHoc.setMaKhoaHoc(txtMaKhoaHoc.getText());
                    khoaHoc.setTenKhoaHoc(txtTenKhoaHoc.getText());     
                    int status = khoaHocDAO.save(khoaHoc);
                    switch (status) {
                        case SUCCESS : {
                            DIALOG_HELPER.infoDialog("Thông báo", "Thêm khóa học thành công!");
                            reloadTable();
                            clearData();
                            khoaHoc = null;
                            break;
                        }
                        case DUPLICATE_RESULT : {
                            DIALOG_HELPER.infoDialog("Lỗi", "Khóa học đã tồn tại!");
                            break;
                        }
                        default:
                            DIALOG_HELPER.infoDialog("Lỗi", "Đã xảy ra lỗi trong quá trình thêm!");
                    }
                }
            }
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
        
    }

    @FXML
    private void btnCapNhatClicked(MouseEvent event) {
        if (LoginDAO.hasPermissionByName("update_khoahoc")) {
            if (khoaHoc != null) {
                boolean isTenKhoaHoc = Validate.requiredTextField(txtTenKhoaHoc, lblErrorTen, "Tên khóa học không được để trống");
                if (isTenKhoaHoc) {
                    khoaHoc.setMaKhoaHoc(txtMaKhoaHoc.getText());
                    khoaHoc.setTenKhoaHoc(txtTenKhoaHoc.getText());
                    int status = khoaHocDAO.update(khoaHoc);
                    if (status == SUCCESS) {
                        reloadTable();
                        clearData();
                        DIALOG_HELPER.infoDialog("Thông báo", "Sửa khóa học thành công!");
                    }
                }
            } else {
                DIALOG_HELPER.infoDialog("Lỗi", "Vui lòng chọn khóa học cần cập nhật!");
            }
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
    }

    @FXML
    private void btnXoaClicked(MouseEvent event) {
        if (LoginDAO.hasPermissionByName("delete_khoahoc")) {
            if (khoaHoc != null) {
                Optional<ButtonType> resultDialog = DIALOG_HELPER.dialogConfirm("Xác nhận", "Bạn có muốn khóa học #ID: " + khoaHoc.getId() + " hay không?");
                if (DIALOG_HELPER.confirmAction(resultDialog)) {
                    int status=khoaHocDAO.delete(khoaHoc);
                    if(status==SUCCESS) 
                    {
                        reloadTable();
                        clearData();
                        DIALOG_HELPER.infoDialog("Thông báo", "Xóa khóa học thành công!");
                    }
                }
            } else {
                DIALOG_HELPER.infoDialog("Lỗi", "Vui lòng chọn khóa học cần xóa!");
            }
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
    }
    
    @FXML
    private void tableKhoaHocClicked(MouseEvent event) {
        if (tableKhoaHoc.getSelectionModel().getSelectedItem() != null)
        {
            khoaHoc = tableKhoaHoc.getSelectionModel().getSelectedItem();
            txtMaKhoaHoc.setText(khoaHoc.getMaKhoaHoc());
            txtTenKhoaHoc.setText(khoaHoc.getTenKhoaHoc());
            txtMaKhoaHoc.setDisable(true);
        }
    }
    
    @FXML
    private void btnClearClicked(MouseEvent event) {
        clearData();
    }
    
    public void loadTable() {
        String[] titles = {"ID", "Mã khoá học", "Tên khoá học"};
        String[] elements = {"id", "maKhoaHoc", "tenKhoaHoc"};
        List<KhoaHoc> listKhoaHocs = khoaHocDAO.getAll();
        TableHelper<KhoaHoc> tableHelper = new TableHelper<>(tableKhoaHoc, titles, elements, listKhoaHocs);
        tableHelper.setModel();
    }
     
    public void reloadTable(){
        tableKhoaHoc.getItems().removeAll();
        List<KhoaHoc> list = khoaHocDAO.getAll();
        ObservableList<KhoaHoc> listData = FXCollections.observableList(list);
        tableKhoaHoc.setItems(listData);
    }
    
    private void clearData() {
        khoaHoc = null;
        txtMaKhoaHoc.clear();
        txtTenKhoaHoc.clear();
        txtMaKhoaHoc.setDisable(false);
        txtTenKhoaHoc.setDisable(false);
        lblErrorMa.setText(null);
        lblErrorTen.setText(null);
        txtMaKhoaHoc.requestFocus();
    }
}
