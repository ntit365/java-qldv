/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import static Common.DialogHelper.DIALOG_HELPER;
import DAO.LoginDAO;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author truon
 */
public class MainMenuController implements Initializable {

    @FXML
    private Pane btnDashboard;
    @FXML
    private Pane btnDoanVien;
    @FXML
    private Pane btnSetting;
    @FXML
    private Pane btnNguoiDung;
    
    public final int DASHBOARD = 1;
    public final int DOANVIEN = 2;
    public final int DOANPHI = 3;
    public final int CHUYENSH = 4;
    public final int NGUOIDUNG = 5;
    public final int CAIDAT = 6;
    @FXML
    private Pane btnDoanPhi;
    @FXML
    private Pane btnChuyenSH;
    @FXML
    private ImageView btnLogout;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void btnDashboard_handleMouseClick(MouseEvent event) throws IOException {
        if (LoginDAO.hasPermissionByName("view_dashboard")) {
            loadPane(DASHBOARD);
        } else {
          DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");  
        } 
    }

    @FXML
    private void btnDoanVienClicked(MouseEvent event) throws IOException {
        if (LoginDAO.hasPermissionByName("view_doanvien") || LoginDAO.hasPermissionByName("view_doanphi") || LoginDAO.hasPermissionByName("view_chuyensh")) {
            loadPane(DOANVIEN);
        } else {
          DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");  
        } 
    }
    
    @FXML
    private void btnDoanPhiClicked(MouseEvent event) throws IOException {
        if (LoginDAO.hasPermissionByName("view_doanphi")) {
            loadPane(DOANPHI);
        } else {
          DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");  
        } 
    }

    @FXML
    private void btnChuyenSHClicked(MouseEvent event) throws IOException {
        if (LoginDAO.hasPermissionByName("view_chuyensh")) {
            loadPane(CHUYENSH);
        } else {
          DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");  
        } 
    }

    @FXML
    private void btnSettingClicked(MouseEvent event) throws IOException {
        if (LoginDAO.hasPermissionByName("view_khoa") || LoginDAO.hasPermissionByName("view_khoahoc") || LoginDAO.hasPermissionByName("view_lop") || LoginDAO.hasPermissionByName("view_nganh")) {
            loadPane(CAIDAT);
        } else {
            DIALOG_HELPER.infoDialog("Lỗi", "Bạn không có quyền truy cập vui liên hệ admin để được cấp quyền!");
        }
        
    }

    @FXML
    private void btnNguoiDung_handleMouseClick(MouseEvent event) throws IOException {
        loadPane(NGUOIDUNG);
    }
    
    @FXML
    private void btnLogoutClicked(MouseEvent event) throws IOException {
        LoginDAO.logout();
        Parent root = FXMLLoader.load(getClass().getResource("/View/Login.fxml"));
        Scene scene = new Scene(root);
        Controller.window.setScene(scene);
    }
    
    private void loadPane(int type) throws IOException {
        switch(type) {
            case 1:{
                Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("View/Dashboard.fxml"));
                Scene scene = new Scene(root);
                Controller.window.setScene(scene);
                break;
            }
            case 2:{
                FXMLLoader loader= new FXMLLoader();
                loader.setLocation(getClass().getResource("/View/DoanVienUI.fxml"));
                Parent root = loader.load();
                Scene scene = new Scene(root);
                DoanVienController doanVienController = loader.getController();
                if (LoginDAO.hasPermissionByName("view_doanvien")) {
                    doanVienController.loadPaneStart(false, false);
                } else if (LoginDAO.hasPermissionByName("view_doanphi")) {
                    doanVienController.loadPaneStart(true, false);
                } else if (LoginDAO.hasPermissionByName("view_chuyensh")) {
                    doanVienController.loadPaneStart(false, true);
                }
                Controller.window.setScene(scene);
                break;
            }
            case 3:{
                FXMLLoader loader= new FXMLLoader();
                loader.setLocation(getClass().getResource("/View/DoanVienUI.fxml"));
                Parent root = loader.load();
                Scene scene = new Scene(root);
                DoanVienController doanVienController = loader.getController();
                doanVienController.loadPaneStart(true, false);
                Controller.window.setScene(scene);
                break;
            }
            case 4:{
                FXMLLoader loader= new FXMLLoader();
                loader.setLocation(getClass().getResource("/View/DoanVienUI.fxml"));
                Parent root = loader.load();
                Scene scene = new Scene(root);
                DoanVienController doanVienController = loader.getController();
                doanVienController.loadPaneStart(false, true);
                Controller.window.setScene(scene);
                break;
            }
            case 5:{
                Parent root = FXMLLoader.load(getClass().getResource("/View/UserUI.fxml"));
                Scene scene = new Scene(root);
                Controller.window.setScene(scene);
                break;
            }
            case 6:{
                Parent root = FXMLLoader.load(getClass().getResource("/View/SettingUI.fxml"));
                Scene scene = new Scene(root);
                Controller.window.setScene(scene);
                break;
            }
        }
    }
    
}
