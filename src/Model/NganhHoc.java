/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author truon
 */
public class NganhHoc {
    private int id;
    private String maNganhHoc;
    private String tenNganhHoc;
    private int idKhoa;
    private String tenKhoa;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaNganhHoc() {
        return maNganhHoc;
    }

    public void setMaNganhHoc(String maNganhHoc) {
        this.maNganhHoc = maNganhHoc;
    }

    public String getTenNganhHoc() {
        return tenNganhHoc;
    }

    public void setTenNganhHoc(String tenNganhHoc) {
        this.tenNganhHoc = tenNganhHoc;
    }

    public int getIdKhoa() {
        return idKhoa;
    }

    public void setIdKhoa(int idKhoa) {
        this.idKhoa = idKhoa;
    }
    
    public String getTenKhoa() {
        return tenKhoa;
    }

    public void setTenKhoa(String tenKhoa) {
        this.tenKhoa = tenKhoa;
    }
}
