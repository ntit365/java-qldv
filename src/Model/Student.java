/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author truon
 */
public class Student {
    private int id;
    private String studentCode;
    private String hoTen;
    private String gioiTinh;
    private String ngaySinh;
    private String ngayVaoDoan;
    private String dienThoai;
    private String diaChi;
    private String chucVu;
    private boolean soDoan;
    private boolean sinhHoat;
    private String idWard;
    private String idDistrict;
    private String idProvince;
    private int idLop;
    private int idKhoaHoc;
    private boolean doanPhi;

    public boolean isDoanPhi() {
        return doanPhi;
    }

    public void setDoanPhi(boolean doanPhi) {
        this.doanPhi = doanPhi;
    }
    

    public int getIdKhoaHoc() {
        return idKhoaHoc;
    }

    public void setIdKhoaHoc(int idKhoaHoc) {
        this.idKhoaHoc = idKhoaHoc;
    }

    public int getIdLop() {
        return idLop;
    }

    public void setIdLop(int idLop) {
        this.idLop = idLop;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudentCode() {
        return studentCode;
    }

    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public String getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(String ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(String gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public String getNgayVaoDoan() {
        return ngayVaoDoan;
    }

    public void setNgayVaoDoan(String ngayVaoDoan) {
        this.ngayVaoDoan = ngayVaoDoan;
    }

    public String getDienThoai() {
        return dienThoai;
    }

    public void setDienThoai(String dienThoai) {
        this.dienThoai = dienThoai;
    }

    public String getChucVu() {
        return chucVu;
    }

    public void setChucVu(String chucVu) {
        this.chucVu = chucVu;
    }

    public boolean isSoDoan() {
        return soDoan;
    }

    public void setSoDoan(boolean soDoan) {
        this.soDoan = soDoan;
    }

    public boolean isSinhHoat() {
        return sinhHoat;
    }

    public void setSinhHoat(boolean sinhHoat) {
        this.sinhHoat = sinhHoat;
    }
    
    public String getDiaChiImport() {
        return this.diaChi;
    }
    
    public void setDiaChiImport()
    {
        this.diaChi = "{'ward': '" + this.idWard +"', 'district': '" + this.idDistrict + "', 'province': '" + this.idProvince + "'}";
    }


    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getIdWard() {
        return idWard;
    }

    public void setIdWard(String idWard) {
        this.idWard = idWard;
    }

    public String getIdDistrict() {
        return idDistrict;
    }

    public void setIdDistrict(String idDistrict) {
        this.idDistrict = idDistrict;
    }

    public String getIdProvince() {
        return idProvince;
    }

    public void setIdProvince(String idProvince) {
        this.idProvince = idProvince;
    }

}
