/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import DAO.KhoaHocDAO;
import Model.KhoaHoc;
import Model.Student;
import java.util.Optional;

/**
 *
 * @author linhnd
 */
public class GenerateStudentCode {
    public static String Generate(Student st)
    {
        int idKhoaHoc = st.getIdKhoaHoc();
        KhoaHocDAO khoaHocDAO = new KhoaHocDAO();
        Optional<KhoaHoc> kh = khoaHocDAO.get(idKhoaHoc);
        int lastCode = kh.get().getLastCode();
        String code = String.format("%03d%07d",idKhoaHoc,lastCode);
        return code;
    }
}
