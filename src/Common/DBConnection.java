/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author truon
 */
public class DBConnection {

    //private static final String URL = "jdbc:mysql://udolink.tk:3306/dv_qldv_final?useUnicode=yes&characterEncoding=UTF-8";
    private static final String URL = "jdbc:mysql://localhost:3306/qldv?useUnicode=yes&characterEncoding=UTF-8";
    //private static final String USERNAME = "haui_qldv";
    private static final String USERNAME = "root";
    //private static final String PASSWORD = "Ntit1998!@#";
    private static final String PASSWORD = "";

    private static Connection cnn = null;   
    public static Connection createConnection(){
        try {
            try {
                Class.forName("com.mysql.jdbc.Driver"); // Load mysql driver
            } catch (ClassNotFoundException ex) {
            }
            cnn = DriverManager.getConnection(URL,USERNAME,PASSWORD);
        } catch (SQLException ex) {
        }
        return cnn;
    }
    
    public static void closeConnection(){
        if (cnn != null) {
            try {
                cnn.close();
            } catch (SQLException ex) {
            }
        }
    }
}
