/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import java.util.LinkedList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author truon
 * @param <T>
 */
public class TableHelper<T> {

    private final TableView<T> tableView;
    private final String[] listTitles;
    private final String[] elementNames;
    private final List<TableColumn<T, Object>> cols;
    private final ObservableList<T> listData;
    private final List<T> list;

    public TableHelper(TableView<T> tableView, String[] listTitles, String[] elementNames, List<T> list) {
        this.tableView = tableView;
        this.listTitles = listTitles;
        this.elementNames = elementNames;
        this.list = list;
        this.cols = new LinkedList<>();
        this.listData = FXCollections.observableList(this.list);
    }
    
    public void setModel() {
        for (String listTitle : listTitles) {
            TableColumn<T, Object> col = new TableColumn<>(listTitle);
            cols.add(col);
        }
        /**
         * set Values for each Column
         */
        for(int i=0;i<cols.size();i++)
        {
            cols.get(i).setCellValueFactory(new PropertyValueFactory<>(elementNames[i]));
        }
        tableView.getColumns().addAll(cols);
        tableView.setItems(listData);
    }
    
}
