/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import java.util.Optional;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author truon
 */
public class DialogHelper {
    
    public static final DialogHelper DIALOG_HELPER = new DialogHelper();
    
    private Dialog<ButtonType> createBasicDialog (final String title, final String message) {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.setTitle(title);
        dialog.setContentText(message);
        dialog.initModality(Modality.APPLICATION_MODAL);
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("Image/logo-doan.png"));
        dialog.initStyle(StageStyle.DECORATED);
        return dialog;
    }
    
    public void infoDialog(String title, String message) {
        Dialog<ButtonType> dialog = createBasicDialog(title, message);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        dialog.showAndWait();
    }
    
    public Optional<ButtonType> dialogConfirm(String title, String message) {
        Dialog<ButtonType> dialog = createBasicDialog(title, message);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.YES);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.NO);
        return dialog.showAndWait();
    }
    
    public boolean confirmAction(Optional<ButtonType> result) {
        return result.get() == ButtonType.YES;
    }
    
    
}
