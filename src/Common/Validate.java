/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

/**
 *
 * @author truon
 */
public class Validate {
    
    public static boolean requiredTextField(TextField textField) {
        boolean status = false;
        if (textField.getText().length() != 0 || !textField.getText().isEmpty()) {
            status = true;
        }
        return status;
    }
    
    public static boolean requiredTextField(TextField textField, Label label, String errorMessage) {
       boolean status = true;
       String message = null;
        if (!requiredTextField(textField)) {
            status = false;
            message = errorMessage;
        }
        label.setText(message);
        label.setTextFill(Color.web("#bc3216"));
        return status;
    }
    
    public static boolean requiredCombobox(ComboBox comboBox) {
        boolean status = false;
        if (!comboBox.getSelectionModel().isEmpty() || !comboBox.getSelectionModel().isSelected(-1) || comboBox.getSelectionModel().getSelectedItem() != null) {
            return true;
        }
        return status;
    }
    
    public static boolean requiredCombobox(ComboBox comboBox, Label label, String errorMessage) {
        boolean status = true;
       String message = null;
        if (!requiredCombobox(comboBox)) {
            status = false;
            message = errorMessage;
        }
        label.setText(message);
        label.setTextFill(Color.web("#bc3216"));
        return status;
    }
}
