# HỆ THỐNG QUẢN LÝ ĐOÀN VIÊN
Version 1.0 build with JavaFX

### Libraries

Danh mục thư viện vào đây để tải về nhé!

| Libraries | Link Download |
| ------ | ------ |
| All JAR | [Download](https://drive.google.com/open?id=1fpGsVdpQSSb1fhoeIOnaHRK44KeLuOVT) |

### Note:
- Thư viện jfoenix add vào secene builder và project
- Không thay đổi đường dẫn database

#### Development
```sh
$ git clone https://gitlab.com/ntit365/java-qldv.git /path/to/proj_dir
$ cd /path/to/proj_dir
$ git branch [yourbranch]
$ git checkout [yourbrach]
```